#!/usr/bin/env bash
export PYTHONPATH='./flaskapp'
python3 ./tests/registrarion_test.py -v
python3 ./tests/authorisation_test.py -v
python3 ./tests/profile_test.py -v
python3 ./tests/account_deletion_test.py -v