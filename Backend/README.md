### Requirements:
Actually, all you need to run this server is

 - docker-compose https://docs.docker.com/compose/install/
 
First initialisation will take some time, but then it will be launching pretty fast.
If you want to run tests or use server without docker and nginx,

 - mongodb https://docs.mongodb.org/manual/installation/ 
 - python3.5+
 
 must be installed.
 
### Run Server with docker and nginx:
- replace Backend/server/config/dummy.* files
- from "Backend" directory run "run_server.sh" script

### Run Server only with flask:
- install flaskapp/requirements.txt by pip
- create folder where to store images (/data/images for example)
- set its path into app.config['UPLOAD_FOLDER'] in App/flaskapp/__init__.py
- run mongod with --dbpath flag (mongod --dbpath /data/images for example)
- run python3 App/wsgi.py