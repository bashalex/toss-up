import datetime

from App import db


class Comment(db.EmbeddedDocument):
    text = db.StringField(max_length=512, required=True)
    author_id = db.StringField(required=True)
    date = db.DateTimeField(default=datetime.datetime.now, required=True)


class Post(db.Document):
    # left card
    left_description = db.StringField(max_length=32, required=True)
    left_image_link = db.StringField(max_length=256, required=True)
    # right card
    right_description = db.StringField(max_length=32, required=True)
    right_image_link = db.StringField(max_length=256, required=True)
    # general
    description = db.StringField(max_length=128, required=True)
    author = db.StringField(max_length=64, required=True)
    left_likes = db.IntField(required=True)
    right_likes = db.IntField(required=True)
    date = db.DateTimeField(default=datetime.datetime.now, required=True)
    # comments
    left_comments = db.ListField(db.EmbeddedDocumentField('Comment'))
    right_comments = db.ListField(db.EmbeddedDocumentField('Comment'))


class Account(db.Document):
    nickname = db.StringField(max_length=32, required=True)
    email = db.StringField(max_length=64, required=True)
    token = db.StringField(max_length=64, required=True, primary_key=True)
    validated = db.BooleanField()
    created = db.DateTimeField(default=datetime.datetime.now, required=True)
    image_link = db.StringField(max_length=256)
    status = db.StringField(max_length=128)
    last_visited_post_id = db.StringField()
    posts = db.ListField(db.ReferenceField(Post))
    commented_posts = db.ListField(db.ReferenceField(Post))
