class Error:

    __messages = {
        # general
        1: "wrong argument",
        100: "account doesn't exist",
        200: "invalid token",
        300: "image extension is not allowed",
        400: "no post found :(",
        500: "please sign up or login to like posts",
        # registration
        2: "nickname already registered",
        3: "email already in use",
        4: "invalid account, please try to start registration again",
        5: "invalid code",
        6: "account is already activated",
        # authorisation
        7: "account is not activated",
        8: "wrong password",
        # new post
        9: "description should contains at least 6 symbols",
        # update status
        10: "status should contains no more than 128 symbols",
        # like post
        11: "post not found"
    }

    def __init__(self, error_code):
        self.code = error_code
        self.message = self.__messages.get(error_code, "unknown error")
