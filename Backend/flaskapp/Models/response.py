from flask import jsonify
from Models.error import Error


class Response:
    data = None
    error = None

    def __init__(self, data=None, error_code=None):
        self.data = data
        if error_code is not None:
            self.error = Error(error_code)

    def to_json(self):
        if self.error is not None:
            return jsonify({"message": self.error.message,
                            "status": "error",
                            "errorCode": self.error.code})
        if self.data is None:
            return jsonify({"status": "ok"})
        return jsonify({"data": self.data, "status": "ok"})
