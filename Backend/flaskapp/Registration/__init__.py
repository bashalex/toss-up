from DBModels.models import *
from Models.response import Response
from werkzeug.utils import secure_filename
from hashlib import sha256
import Utils
import os


def register_account(email, nickname, password, image, app, mail) -> Response:
    if Account.objects(nickname=nickname):  # account already exists
        return Response(error_code=2)
    if Account.objects(email=email):  # account already exists
        return Response(error_code=3)
    token = generate_token(nickname, password)
    image_link = None
    if image:
        if not Utils.allowed_file(image.filename, app):
            return Response(error_code=300)
        filename = secure_filename(nickname) + "." + image.filename.rsplit('.', 1)[1]
        image.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        image_link = filename
    Account(nickname=nickname,
            email=email,
            token=token,
            image_link=image_link).save()
    return Response({"token": token})


def generate_token(nickname, password):
    return sha256(str(nickname + password + "trololo").encode()).hexdigest()


def validate_code(token, code) -> Response:
    account = Account.objects(token=token)
    if not account:
        return Response(error_code=4)
    acc = account.get()
    if acc.validated:
        return Response(error_code=6)
    if code != '1234':
        return Response(error_code=5)
    acc.validated = True
    acc.save()
    return Response()


def delete_account(token) -> Response:
    account = Account.objects(token=token)
    if not account:
        return Response(error_code=100)
    else:
        account.delete()
    return Response()
