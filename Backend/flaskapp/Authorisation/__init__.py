import hashlib
from DBModels.models import Account
from Models.response import Response


def login(token) -> Response:
    account = Account.objects(token=token)
    if not account:
        return Response(error_code=100)
    acc = account.get()
    if not acc.validated:
        return Response(error_code=7)
    response = {"email": acc.email,
                "nickname": acc.nickname,
                "status": acc.status,
                "image_link": acc.image_link,
                "last_visited_post": acc.last_visited_post_id,
                "created": acc.created}
    print("authorisation... OK")
    return Response(response)


def generate_token(nickname, password):
    return hashlib.sha256(str(nickname + password).encode()).hexdigest()


def get_token(nickname, password) -> Response:
    account = Account.objects(nickname=nickname)
    if not account:
        return Response(error_code=100)
    acc = account.get()
    if generate_token(nickname, password) != acc.token:
        return Response(error_code=8)
    return Response({"token": acc.token})
