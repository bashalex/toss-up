from DBModels.models import Post, Comment, Account
from Models.response import Response
import random


def get_comments(post_id, left) -> Response:
    comments = Comment.objects(_id=post_id, left=left)
    return Response(comments)


def get_next_post() -> Response:
    # TODO: here should be an algorithm for sorting
    # at least check that post wasn't created by current
    number_of_posts = Post.objects().count()
    if number_of_posts == 0:
        return Response(error_code=400)
    post = Post.objects()[random.randint(0, number_of_posts-1)]
    account = Account.objects(token=post.author)
    if not account:
        author = {"nickname": "deleted", "image_link": None}
    else:
        acc = account.get()
        author = {"nickname": acc.nickname, "image_link": acc.image_link}
    return Response({
        "author": author,
        "description": post.description,
        "id": str(post.id),
        "left_card": {
            "image_link": post.left_image_link,
            "description": post.left_description,
            "comments": post.left_comments,
            "number_of_likes": post.left_likes
        },
        "right_card": {
            "image_link": post.right_image_link,
            "description": post.right_description,
            "comments": post.right_comments,
            "number_of_likes": post.right_likes
        }
    })


def like_and_get_next_post(token, post_id, is_left) -> Response:
    account = Account.objects(token=token)
    if not account:
        return Response(error_code=500)
    post = Post.objects(id=post_id)
    if not post:
        return Response(error_code=11)
    p = post.get()
    if is_left:
        p.left_likes += 1
    else:
        p.right_likes += 1
    p.save()
    return get_next_post()


def save_comment(token, post_id, left, text) -> Response:
    return Response()

