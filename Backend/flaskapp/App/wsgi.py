from App import routes

app = routes.app

if __name__ == "__main__":
    routes.app.run(host="0.0.0.0")
