from flask import Flask
from flask_mail import Mail
import os
from flask_mongoengine import MongoEngine

app = Flask(__name__)
app.config['MONGODB_SETTINGS'] = {
    'db': 'TossUp',
    'host': os.environ.get('DB_PORT_27017_TCP_ADDR', "localhost"),
    'port': 27017
}
app.config['UPLOAD_FOLDER'] = '/data/images'
app.config['ALLOWED_EXTENSIONS'] = {'png', 'jpg', 'jpeg'}

db = MongoEngine(app)
mail = Mail(app)

app.debug = True
