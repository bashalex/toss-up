import Registration
from Models.response import Response
from DBModels.models import *
from flask import request, send_from_directory
from App import app, mail
import Authorisation
import Profile
import Feed
import os


@app.route('/')
def main():
    response = Response("Welcome to the TossUp server!")
    return response.to_json()


@app.route('/registration', methods=['POST'])
def register_account():
    email = request.form.get('email').replace("\"", "")
    nickname = request.form.get('nickname').replace("\"", "")
    password = request.form.get('password').replace("\"", "")
    image = request.files.get('photo')
    return Registration.register_account(email, nickname, password, image, app, mail).to_json()


@app.route('/registration/delete', methods=['GET'])
def delete_account():
    token = request.headers.get('token')
    return Registration.delete_account(token).to_json()


@app.route('/registration/validate', methods=['GET'])
def validate_code():
    token = request.headers.get('token')
    code = request.args.get('code')
    return Registration.validate_code(token, code).to_json()


@app.route('/authorisation', methods=['GET'])
def login():
    token = request.headers.get('token')
    return Authorisation.login(token).to_json()


@app.route('/authorisation/get_token', methods=['GET'])
def get_token():
    nickname = request.headers.get('nickname')
    password = request.headers.get('password')
    return Authorisation.get_token(nickname, password).to_json()


@app.route('/profile', methods=['GET'])
def get_profile():
    token = request.headers.get('token')
    nickname = request.args.get('nickname')
    return Profile.get_profile(token, nickname).to_json()


@app.route('/profile/posts', methods=['GET'])
def get_user_posts():
    token = request.headers.get('token')
    return Profile.get_posts(token).to_json()


@app.route('/profile/update_status', methods=['POST'])
def update_status():
    token = request.headers.get('token')
    status = request.form.get('status').replace("\"", "")
    return Profile.update_status(token, status).to_json()


@app.route('/profile/update_photo', methods=['POST'])
def update_photo():
    token = request.headers.get('token')
    photo = request.files.get('photo')
    return Profile.update_photo(token, photo, app).to_json()


@app.route('/feed/next_post', methods=['GET'])
def get_next_post():
    return Feed.get_next_post().to_json()


@app.route('/feed/like_and_next_post', methods=['GET'])
def like_and_get_next_post():
    token = request.headers.get('token')
    post_id = request.args.get('post_id')
    is_left_card = request.args.get('is_left')
    is_left_card = True if is_left_card == "true" else False
    return Feed.like_and_get_next_post(token, post_id, is_left_card).to_json()


@app.route('/feed/new', methods=['POST'])
def new_post():
    token = request.headers.get('token')
    left_description = request.form.get('left_description').replace("\"", "")
    left_image = request.files.get('left_image')
    right_description = request.form.get('right_description').replace("\"", "")
    right_image = request.files.get('right_image')
    description = request.form.get('description').replace("\"", "")
    return Profile.save_post(token, left_description, left_image,
                             right_description, right_image, description, app).to_json()


@app.route('/test_drop_data', methods=['GET'])
def test_drop_data():
    # TODO: remove this method before production
    # used only for tests
    Account().drop_collection()
    print("delete all accounts... OK")
    Post().drop_collection()
    print("delete all posts... OK")
    test_files = os.listdir(app.config['UPLOAD_FOLDER'])
    for f in test_files:
        os.remove(os.path.join(app.config['UPLOAD_FOLDER'], f))
        print("remove {}... OK".format(f))
    return Response().to_json()


@app.route('/image/<path:path>')
def send_image(path):
    # TODO: Nginx should do that, not flask
    return send_from_directory(app.config['UPLOAD_FOLDER'], path)


@app.route('/feed/comments', methods=['GET'])
def get_comments():
    # TODO: doesn't work
    token = request.args.get('token')
    post_id = request.args.get('post')
    left = request.args.get('left')
    return Feed.get_comments(post_id, left).to_json()


@app.route('/feed/comments/new', methods=['POST'])
def new_comment():
    # TODO: doesn't work
    token = request.form.get('token')
    post_id = request.form.get('post')
    left = request.form.get('left')
    text = request.form.get('text')
    return Feed.save_comment(token, post_id, left, text).to_json()
