from DBModels.models import Account, Post
from Models.response import Response
from datetime import datetime
from hashlib import sha1
import Utils
import os


def get_profile(token, nickname) -> Response:
    if not Account.objects(token=token):
        return Response(error_code=200)
    account = Account.objects(nickname=nickname)
    if not account:
        return Response(error_code=100)
    acc = account.get()
    response = {"email": acc.email, "nickname": acc.nickname, "status": acc.status,
                "created": acc.created, "image_link": acc.image_link}
    return Response(response)


def save_post(token, left_description, left_image,
              right_description, right_image, description, app) -> Response:
    if not Account.objects(token=token):
        return Response(error_code=200)
    if not left_image or not right_image:
        return Response(error_code=1)
    if not Utils.allowed_file(left_image.filename, app) or not Utils.allowed_file(right_image.filename, app):
        return Response(error_code=300)
    if not description or len(description) < 6:
        return Response(error_code=9)
    left_image_link = sha1(str('left' + description + str(datetime.now())).encode()).hexdigest() + \
                      "." + left_image.filename.rsplit('.', 1)[1]
    right_image_link = sha1(str('right' + description + str(datetime.now())).encode()).hexdigest() + \
                       "." + right_image.filename.rsplit('.', 1)[1]
    left_image.save(os.path.join(app.config['UPLOAD_FOLDER'], left_image_link))
    right_image.save(os.path.join(app.config['UPLOAD_FOLDER'], right_image_link))
    post = Post(left_description=left_description,
                left_image_link=left_image_link,
                right_description=right_description,
                right_image_link=right_image_link,
                description=description,
                author=token,
                left_likes=0,
                right_likes=0)
    post.save()
    Account(token=token).update(push__posts=post)
    return Response()


def get_posts(token) -> Response:
    # TODO: generally this method should returns only few last posts, but who cares
    # TODO: author field should be removed from response
    account = Account.objects(token=token)
    if not account:
        return Response(error_code=200)
    acc = account.get()
    author = {"nickname": acc.nickname, "image_link": acc.image_link}
    posts = []
    for post in acc.posts:
        posts.append({
            "author": author,
            "description": post.description,
            "id": str(post.id),
            "left_card": {
                "image_link": post.left_image_link,
                "description": post.left_description,
                "comments": post.left_comments,
                "number_of_likes": post.left_likes
            },
            "right_card": {
                "image_link": post.right_image_link,
                "description": post.right_description,
                "comments": post.right_comments,
                "number_of_likes": post.right_likes
            }
        })
    return Response({"posts": posts})


def update_status(token, status) -> Response:
    account = Account.objects(token=token)
    if not account:
        return Response(error_code=200)
    if not status or len(status) >= 128:
        return Response(error_code=10)
    acc = account.get()
    acc.status = status
    acc.save()
    return Response()


def update_photo(token, photo, app) -> Response:
    account = Account.objects(token=token)
    if not account:
        return Response(error_code=200)
    image_link = sha1(str(token + str(datetime.now())).encode()).hexdigest() + \
                      "." + photo.filename.rsplit('.', 1)[1]
    photo.save(os.path.join(app.config['UPLOAD_FOLDER'], image_link))
    acc = account.get()
    acc.image_link = image_link
    acc.save()
    response = {"email": acc.email,
                "nickname": acc.nickname,
                "status": acc.status,
                "image_link": image_link,
                "last_visited_post": acc.last_visited_post_id,
                "created": acc.created}
    return Response(response)
