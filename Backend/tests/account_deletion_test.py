import unittest
import warnings
from flask_test import FlaskTest


class TestAccountDeletion(FlaskTest):

    def test_delete_account(self):
        self.prGreen("\n#TEST 1")
        self.prYellow("Account deletion")
        warnings.simplefilter("ignore")
        response = self.app.get('/registration/delete',
                                headers={'token': self.token})
        self.assertEqual(response.status_code, 200)
        data = self.read_response(response)
        self.assertEqual(data['status'], 'ok')
        self.prYellow("First account deletion... OK")

        response = self.app.get('/registration/delete',
                                headers={'token': self.token1})
        self.assertEqual(response.status_code, 200)
        data = self.read_response(response)
        self.assertEqual(data['status'], 'ok')
        self.prYellow("Second account deletion... OK")

        response = self.app.get('/registration/delete',
                                headers={'token': "1234567"})
        self.assertEqual(response.status_code, 200)
        data = self.read_response(response)
        self.assertEqual(data['status'], 'error')
        self.assertEqual(data['errorCode'], 100)
        self.prYellow("Nonexistent account... OK")

    def test_delete_all_test_data(self):
        self.prGreen("\n#TEST 2")
        self.prYellow("Delete all uploaded files")
        response = self.app.get('/test_drop_data')
        self.assertEqual(response.status_code, 200)

if __name__ == '__main__':
    unittest.main()
