import unittest
import warnings
from flask_test import FlaskTest


class TestAuthorisation(FlaskTest):

    def test_authorisation(self):
        self.prGreen("\n#TEST 1")
        self.prYellow("Authorisation")
        warnings.simplefilter("ignore")
        response = self.app.get('/authorisation',
                                headers={'token': self.token})
        self.assertEqual(response.status_code, 200)
        data = self.read_response(response)
        self.assertEqual(data['status'], 'ok')
        self.prYellow("Correct account... OK")

        response = self.app.get('/authorisation',
                                headers={'token': self.token1})
        self.assertEqual(response.status_code, 200)
        data = self.read_response(response)
        self.assertEqual(data['status'], 'error')
        self.assertEqual(data['errorCode'], 7)
        self.prYellow("Non-activated account... OK")

        response = self.app.get('/authorisation',
                                headers={'token': "1234567"})
        self.assertEqual(response.status_code, 200)
        data = self.read_response(response)
        self.assertEqual(data['status'], 'error')
        self.assertEqual(data['errorCode'], 100)
        self.prYellow("Nonexistent account... OK")

    def test_authorisation_get_token(self):
        self.prGreen("\n#TEST 2")
        self.prYellow("Get token by nickname and password")
        response = self.app.get('/authorisation/get_token',
                                headers={'nickname': self.nickname, "password": self.password})
        self.assertEqual(response.status_code, 200)
        data = self.read_response(response)
        self.assertEqual(data['status'], 'ok')
        self.prYellow("Correct account... OK")

        response = self.app.get('/authorisation/get_token',
                                headers={'nickname': self.nickname, "password": "adasdnk"})
        self.assertEqual(response.status_code, 200)
        data = self.read_response(response)
        self.assertEqual(data['status'], 'error')
        self.assertEqual(data['errorCode'], 8)
        self.prYellow("Wrong password... OK")

        response = self.app.get('/authorisation/get_token',
                                headers={'nickname': "sdfvia", "password": self.password})
        self.assertEqual(response.status_code, 200)
        data = self.read_response(response)
        self.assertEqual(data['status'], 'error')
        self.assertEqual(data['errorCode'], 100)
        self.prYellow("Nonexistent account... OK")

if __name__ == '__main__':
    unittest.main()
