import unittest
import warnings
import io
import os
from flask_test import FlaskTest


class TestRegistration(FlaskTest):

    def test_accounts_registration(self):
        self.prGreen("\n#TEST 1")
        self.prYellow("Registration")
        params = {
            "email": "bash.test2@yandex.ru",
            "nickname": self.nickname,
            "password": self.password
        }
        warnings.simplefilter("ignore")
        response = self.app.post('/registration', data=params)
        self.assertEqual(response.status_code, 200)
        data = self.read_response(response)
        self.assertEqual(data['status'], 'ok')
        self.prYellow("First correct account... OK")

        params["email"] = "bash.test3@yandex.ru"
        response = self.app.post('/registration', data=params)
        data = self.read_response(response)
        self.assertEqual(data['status'], 'error')
        self.assertEqual(data['errorCode'], 2)
        self.prYellow("Account with the same nickname... OK")

        params["email"] = "bash.test2@yandex.ru"
        params["nickname"] = "test2"
        response = self.app.post('/registration', data=params)
        data = self.read_response(response)
        self.assertEqual(data['status'], 'error')
        self.assertEqual(data['errorCode'], 3)
        self.prYellow("Account with the same email... OK")

        print(os.listdir('.'))
        with open('images/test_face.png', 'rb') as img1:
            image = io.BytesIO(img1.read())
        params = {
            "email": "bashalex@yandex.ru",
            "nickname": self.nickname1,
            "password": self.password1,
            'photo': (image, 'test_face.png')
        }
        warnings.simplefilter("ignore")
        response = self.app.post('/registration', data=params)
        self.assertEqual(response.status_code, 200)
        data = self.read_response(response)
        self.assertEqual(data['status'], 'ok')
        self.prYellow("Second correct account with photo... OK")

    def test_code_validation(self):
        self.prGreen("\n#TEST 2")
        self.prYellow("Code validation")
        code = 5555
        response = self.app.get('/registration/validate?code={}'.format(code),
                                headers={'token': self.token})
        self.assertEqual(response.status_code, 200)
        data = self.read_response(response)
        self.assertEqual(data['status'], 'error')
        self.assertEqual(data['errorCode'], 5)
        self.prYellow("Invalid code... OK")

        code = 1234
        response = self.app.get('/registration/validate?code={}'.format(code),
                                headers={'token': "123456789"})
        self.assertEqual(response.status_code, 200)
        data = self.read_response(response)
        self.assertEqual(data['status'], 'error')
        self.assertEqual(data['errorCode'], 4)
        self.prYellow("Invalid token... OK")

        code = 1234
        warnings.simplefilter("ignore")
        response = self.app.get('/registration/validate?code={}'.format(code),
                                headers={'token': self.token})
        self.assertEqual(response.status_code, 200)
        data = self.read_response(response)
        self.assertEqual(data['status'], 'ok')
        self.prYellow("Correct code... OK")

        code = 1234
        warnings.simplefilter("ignore")
        response = self.app.get('/registration/validate?code={}'.format(code),
                                headers={'token': self.token})
        self.assertEqual(response.status_code, 200)
        data = self.read_response(response)
        self.assertEqual(data['status'], 'error')
        self.assertEqual(data['errorCode'], 6)
        self.prYellow("Already activated account... OK")


if __name__ == '__main__':
    unittest.main()
