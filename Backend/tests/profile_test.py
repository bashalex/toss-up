import unittest
import warnings
import io
from flask_test import FlaskTest


class TestProfile(FlaskTest):

    def test_get_profile(self):
        self.prGreen("\n#TEST 1")
        self.prYellow("Profile")
        response = self.app.get('/profile?nickname={}'.format(self.nickname1),
                                headers={'token': self.token})
        self.assertEqual(response.status_code, 200)
        data = self.read_response(response)
        self.assertEqual(data['status'], 'ok')
        self.prYellow("Correct account... OK")

        response = self.app.get('/profile?nickname={}'.format("qwerty"),
                                headers={'token': self.token})
        self.assertEqual(response.status_code, 200)
        data = self.read_response(response)
        self.assertEqual(data['status'], 'error')
        self.assertEqual(data['errorCode'], 100)
        self.prYellow("Non-existed account... OK")

        response = self.app.get('/profile?nickname={}'.format(self.nickname1),
                                headers={'token': "1234"})
        self.assertEqual(response.status_code, 200)
        data = self.read_response(response)
        self.assertEqual(data['status'], 'error')
        self.assertEqual(data['errorCode'], 200)
        self.prYellow("Invalid token... OK")

    def test_create_post(self):
        self.prGreen("\n#TEST 2")
        self.prYellow("Create post")
        with open('images/light_side.png', 'rb') as img:
            left_image = io.BytesIO(img.read())
        with open('images/dark_side.jpg', 'rb') as img:
            right_image = io.BytesIO(img.read())
        params = {
            "left_description": "Light Side",
            "left_image": (left_image, 'light_side.png'),
            "right_description": "Dark Side",
            "right_image": (right_image, 'dark_side.jpg'),
            "description": "Which side are you on?"
        }
        warnings.simplefilter("ignore")
        response = self.app.post('/feed/new', data=params, headers={'token': self.token})
        self.read_response(response)

    def test_get_posts(self):
        self.prGreen("\n#TEST 3")
        self.prYellow("Get posts")
        response = self.app.get('/profile/posts',
                                headers={'token': self.token})

        self.assertEqual(response.status_code, 200)
        self.read_response(response)

    def test_update_status(self):
        self.prGreen("\n#TEST 4")
        self.prYellow("Update status")
        print("\tProfile with old status:")
        response = self.app.get('/profile?nickname={}'.format(self.nickname),
                                headers={'token': self.token})
        self.assertEqual(response.status_code, 200)
        self.read_response(response)
        print("\tUpdate status...")
        params = {
                    "status": "New test status"
                }
        warnings.simplefilter("ignore")
        response = self.app.post('profile/update_status', data=params, headers={'token': self.token})
        self.assertEqual(response.status_code, 200)
        self.read_response(response)
        print("\tProfile after update:")
        response = self.app.get('/profile?nickname={}'.format(self.nickname),
                                headers={'token': self.token})
        self.assertEqual(response.status_code, 200)
        data = self.read_response(response)
        self.assertEqual(data['status'], "ok")
        self.assertEqual(data['data']['status'], "New test status")
        self.prYellow("Status updated... OK")


if __name__ == '__main__':
    unittest.main()
