import unittest
import hashlib
import json
from App import routes


class FlaskTest(unittest.TestCase):

    # 1 account
    nickname = "test"
    password = "0123456789ABCDEF"
    token = hashlib.sha256(str(nickname + password).encode()).hexdigest()
    # 2 account
    nickname1 = "tester"
    password1 = "0123456789ABCDEF"
    token1 = hashlib.sha256(str(nickname1 + password1).encode()).hexdigest()

    def setUp(self):
        self.app = routes.app.test_client()

    def prRed(self, prt): print("\033[91m {}\033[00m".format(prt))

    def prGreen(self, prt): print("\033[92m {}\033[00m".format(prt))

    def prYellow(self, prt): print("\033[93m {}\033[00m".format(prt))

    def read_response(self, response):
        data = json.loads(response.data.decode("utf-8"))
        print(data)
        return data
