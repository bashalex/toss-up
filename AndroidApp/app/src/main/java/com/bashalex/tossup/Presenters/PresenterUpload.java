package com.bashalex.tossup.Presenters;

import android.net.Uri;
import android.support.v4.app.Fragment;
import android.widget.EditText;

import com.bashalex.tossup.API.DefaultResponse;
import com.bashalex.tossup.Custom.TossUpPresenter;
import com.bashalex.tossup.Interfaces.ActivityRequestedListener;
import com.bashalex.tossup.Interfaces.QuestionUploadedListener;
import com.bashalex.tossup.Models.Authorisation;
import com.bashalex.tossup.Models.Card;
import com.bashalex.tossup.Models.Post;
import com.bashalex.tossup.Models.ServerRequests;
import com.bashalex.tossup.Models.Utils;
import com.bashalex.tossup.R;

import java.io.File;

import rx.Subscription;
import rx.observables.ConnectableObservable;

/**
 * Created by Alex Bash on 01.03.16.
 */
public class PresenterUpload extends TossUpPresenter {

    private Subscription subscr;
    private QuestionUploadedListener questionUploadedListener;

    public void upload(EditText leftDescription, EditText rightDescription, EditText question,
                       Uri leftImage, Uri rightImage) {
        if (!imagesValid(leftImage, rightImage)) {
            mActivity.showToast(mActivity.getString(R.string.image_not_loaded));
            return;
        }
        if (question.getText().toString().isEmpty()) {
            mActivity.showToast(mActivity.getString(R.string.empty_question));
            return;
        }
        Card leftCard = new Card(Utils.getPath(mActivity, leftImage), leftDescription.getText().toString(), 0);
        Card rightCard = new Card(Utils.getPath(mActivity, rightImage), rightDescription.getText().toString(), 0);
        Post post = new Post(leftCard, rightCard, question.getText().toString(), null, null);
        mActivity.showProgress();
        ConnectableObservable<DefaultResponse<Object>> connection =
                ServerRequests.uploadPost(mActivity, post, Authorisation.getToken(mActivity));
        if (subscr != null && !subscr.isUnsubscribed()) subscr.unsubscribe();
        subscr = connection.filter(response -> response.getErrorCode() == 0)
                .subscribe(response -> {
                    mActivity.showToast(mActivity.getString(R.string.upload_success));
                    questionUploadedListener.onQuestionUploaded();
                });
        connection.connect();
    }

    private boolean imagesValid(Uri left, Uri right) {
        if (left == null || right == null) return false;
        String path1 = Utils.getPath(mActivity, left);
        String path2 = Utils.getPath(mActivity, right);
        printLog(path1 + "\n" + path2);
        File f1 = new File(path1);
        File f2 = new File(path2);
        printLog(String.format("f1: %s", f1.exists()));
        printLog(String.format("f2: %s", f2.exists()));
        return !(!f1.exists() || !f2.exists());
    }

    public void setListeners(Fragment fragment) {
        if (fragment instanceof QuestionUploadedListener) {
            this.questionUploadedListener = (QuestionUploadedListener) fragment;
        } else {
            throw new RuntimeException(fragment.toString()
                    + " must implement QuestionUploadedListener");
        }
    }

    public void onDestroy() {
        questionUploadedListener = null;
        super.onDestroy();
    }

}
