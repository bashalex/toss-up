package com.bashalex.tossup.Models;

import android.net.Uri;

import com.bashalex.tossup.API.APIClient;
import com.bashalex.tossup.API.AccountResponse;
import com.bashalex.tossup.API.DefaultResponse;
import com.bashalex.tossup.API.PostsResponse;
import com.bashalex.tossup.API.TokenResponse;
import com.bashalex.tossup.Custom.TossUpActivity;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.observables.ConnectableObservable;
import rx.schedulers.Schedulers;

/**
 * Created by Alex Bash on 18.02.16.
 */
public class ServerRequests {

    private static Subscription  subscr;

    private static <T> ConnectableObservable<DefaultResponse<T>> response(TossUpActivity context, Observable<DefaultResponse<T>> connection) {
        ConnectableObservable<DefaultResponse<T>> c =
                connection
                .subscribeOn(Schedulers.io())
                .doOnCompleted(context::dismissProgress)
                .onErrorResumeNext(Observable.just(new DefaultResponse<>(null, null, -1, "Can't connect to the server. Please check your internet connection")))
                .doOnError(throwable -> {
                    throwable.printStackTrace();
                    context.dismissProgress();
                })
                .observeOn(AndroidSchedulers.mainThread())
                .publish();
        if (subscr != null && !subscr.isUnsubscribed()) subscr.unsubscribe();
        subscr = c.filter(response -> response.getErrorCode() != 0)
                .subscribe(response -> {
                    context.showToast(response.getMessage());
                    context.dismissProgress();
                });
        return c;
    }

    public static ConnectableObservable<DefaultResponse<Post>> getNextPost(TossUpActivity context, String token) {
        return response(context, APIClient.getAPIClient(context)
                                          .getNextPost(token));
    }

    public static ConnectableObservable<DefaultResponse<Post>> likeAndGetNextPost(TossUpActivity context, String token, String currentPostId, boolean isLeft) {
        return response(context, APIClient.getAPIClient(context)
                .likeAndGetNextPost(token, currentPostId, isLeft));
    }

    public static ConnectableObservable<DefaultResponse<TokenResponse>> registerNewAccount(TossUpActivity context, String email,
                                                                                String nickname, String password,
                                                                                Uri image) {
        return response(context, APIClient.getAPIClient(context)
                .registerNewAccount(email, nickname, password,
                        image != null ? RequestBody.create(MediaType.parse("image/*"), new File(Utils.getPath(context, image)))
                                : null));
    }

    public static ConnectableObservable<DefaultResponse<Object>> validateCode(TossUpActivity context, String code) {
        return response(context, APIClient.getAPIClient(context)
                .validateEmail(Authorisation.getToken(context), code));
    }

    public static ConnectableObservable<DefaultResponse<AccountResponse>> login(TossUpActivity context, String token) {
        return response(context, APIClient.getAPIClient(context)
                .login(token));
    }

    public static ConnectableObservable<DefaultResponse<TokenResponse>> getToken(TossUpActivity context,
                                                                      String nickname, String password) {
        return response(context, APIClient.getAPIClient(context)
                .getToken(nickname, password));
    }

    public static ConnectableObservable<DefaultResponse<Object>> uploadPost(TossUpActivity context, Post post, String token) {
        return response(context, APIClient.getAPIClient(context)
                .createPost(post.getLeftCard().getDescription(),
                        RequestBody.create(MediaType.parse("image/*"), new File(post.getLeftCard().getImageLink())),
                        post.getRightCard().getDescription(),
                        RequestBody.create(MediaType.parse("image/*"), new File(post.getRightCard().getImageLink())),
                        post.getDescription(),
                        token
                ));
    }

    public static ConnectableObservable<DefaultResponse<PostsResponse>> getUserPosts(TossUpActivity context, String token) {
        return response(context, APIClient.getAPIClient(context)
                .getUserPosts(token));
    }

    public static ConnectableObservable<DefaultResponse<Object>> updateStatus(TossUpActivity context, String newStatus, String token) {
        return response(context, APIClient.getAPIClient(context)
                .updateStatus(token, newStatus));
    }

    public static ConnectableObservable<DefaultResponse<AccountResponse>> updatePhoto(TossUpActivity context, Uri image, String token) {
        return response(context, APIClient.getAPIClient(context)
                .updatePhoto(token, RequestBody.create(MediaType.parse("image/*"), new File(Utils.getPath(context, image)))));
    }

    public static List<Comment> getComments(Post post, boolean left) {
        //        TODO: implement method
        return null;
    }
}
