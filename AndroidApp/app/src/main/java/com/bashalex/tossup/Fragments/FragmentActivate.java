package com.bashalex.tossup.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.bashalex.tossup.Custom.TossUpFragment;
import com.bashalex.tossup.Presenters.PresenterActivate;
import com.bashalex.tossup.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import nucleus.factory.RequiresPresenter;

@RequiresPresenter(PresenterActivate.class)
public class FragmentActivate extends TossUpFragment<PresenterActivate> {

    @Bind(R.id.activate_code) EditText codeField;

    public FragmentActivate() {
        // Required empty public constructor
    }

    public static FragmentActivate newInstance() {
        return new FragmentActivate();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_activate, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.activate_button)
    public void register() {
        String code = codeField.getText().toString();
        getPresenter().activate(code);
    }

    @OnClick(R.id.back_button)
    public void back() {
        getPresenter().back();
    }

}
