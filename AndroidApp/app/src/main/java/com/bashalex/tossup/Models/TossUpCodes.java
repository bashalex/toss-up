package com.bashalex.tossup.Models;

/**
 * Created by Alex Bash on 01.03.16.
 */
public class TossUpCodes {
    public final static int leftCardGallery = 154;
    public final static int rightCardGallery = 155;
    public final static int avatarGallery = 157;
    public final static int leftCardCamera = 173;
    public final static int rightCardCamera = 174;
    public final static int avatarCamera = 217;
    public final static int getCameraPermissions = 169;
    public final static int getGalleryPermissions = 170;
}
