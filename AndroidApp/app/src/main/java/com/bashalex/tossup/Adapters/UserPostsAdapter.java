package com.bashalex.tossup.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bashalex.tossup.Models.Post;
import com.bashalex.tossup.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Alex Bash on 14.06.16.
 */
public class UserPostsAdapter extends RecyclerView.Adapter<UserPostsAdapter.ViewHolder> {

    public List<Post> mPosts;
    private Context mContext;

    public UserPostsAdapter(Context context, List<Post> posts) {
        mContext = context;
        mPosts = posts;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_question, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Post post = mPosts.get(position);
        holder.post = post;
        holder.questionTitle.setText(post.getDescription());
        holder.leftNumber.setText(String.valueOf(post.getLeftCard().getNumberOfLikes()));
        Picasso.with(mContext)
                .load("http://" + mContext.getString(R.string.server) + "/image/" +
                        post.getLeftCard().getImageLink())
                .resize(400, 400)
                .centerCrop()
                .into(holder.leftImage);
        holder.rightNumber.setText(String.valueOf(post.getRightCard().getNumberOfLikes()));
        Picasso.with(mContext)
                .load("http://" + mContext.getString(R.string.server) + "/image/" +
                        post.getRightCard().getImageLink())
                .resize(400, 400)
                .centerCrop()
                .into(holder.rightImage);
        if (post.getLeftCard().getNumberOfLikes() > post.getRightCard().getNumberOfLikes()) {
            holder.rightShadow.setAlpha(0.7f);
            holder.leftShadow.setAlpha(0);
        } else if (post.getLeftCard().getNumberOfLikes() < post.getRightCard().getNumberOfLikes()) {
            holder.rightShadow.setAlpha(0);
            holder.leftShadow.setAlpha(0.7f);
        } else {
            holder.rightShadow.setAlpha(0);
            holder.leftShadow.setAlpha(0);
        }
    }

    @Override
    public int getItemCount() {
        return mPosts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View view;
        public Post post;
        @Bind(R.id.question_title)
        TextView questionTitle;
        @Bind(R.id.left_shadow)
        View leftShadow;
        @Bind(R.id.left_number)
        TextView leftNumber;
        @Bind(R.id.left_card_image)
        ImageView leftImage;
        @Bind(R.id.right_shadow)
        View rightShadow;
        @Bind(R.id.right_number)
        TextView rightNumber;
        @Bind(R.id.right_card_image)
        ImageView rightImage;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            this.view = view;
        }
    }
}
