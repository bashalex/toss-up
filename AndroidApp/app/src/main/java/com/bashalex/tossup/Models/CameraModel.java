package com.bashalex.tossup.Models;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.util.Log;

import com.bashalex.tossup.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Locale;

public class CameraModel {

    public static final int CAMERA_FRONT = 0;
    public static final int CAMERA_BACK = 1;

    public static final int STATE_PREVIEW = 0;
    public static final int STATE_WAITING_LOCK = 1;
    public static final int STATE_WAITING_PRECAPTURE = 2;
    public static final int STATE_WAITING_NON_PRECAPTURE = 3;
    public static final int STATE_PICTURE_TAKEN = 4;

    private static int lastFileNumber = 0;

    public static Uri saveImage(byte[] image, Context c) {
        File file = getFile(c);
        try {
            OutputStream fOut = new FileOutputStream(file);
            fOut.write(image);
            fOut.close();
            return Uri.fromFile(file);
        } catch (Exception e) {
            Log.e("e", e.getMessage());
        }
        return null;
    }

    public static Uri saveImageFromOldCamera(byte[] data, Context c, boolean cameraFront) {
        try {
            Bitmap bm = null;
            // Converting ByteArray to Bitmap - >Rotate and Convert back to Data
            if (data != null) {
                bm = BitmapFactory.decodeByteArray(data, 0, data.length);
                Matrix mtx = new Matrix();
                if (cameraFront) {
                    mtx.postRotate(270);
                } else {
                    mtx.postRotate(90);
                }

                bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), mtx, true);
            }
            if (bm == null) return null;
            File file = getFile(c);
            OutputStream fOut = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
            bm.recycle();
            return Uri.fromFile(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static File getFile(Context c) {
        String name = String.format(Locale.ENGLISH, "%s%d.png",
                c.getString(R.string.temp_image), lastFileNumber);
        String folderToSave = c.getCacheDir().toString();
        File file = new File(folderToSave, name);
        if (file.exists()) {
            ++lastFileNumber;
            file = getFile(c);
        }
        return file;
    }
}
