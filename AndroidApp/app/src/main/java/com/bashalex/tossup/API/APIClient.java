package com.bashalex.tossup.API;

import android.content.Context;
import android.util.Log;

import com.bashalex.tossup.Interfaces.RequestsAPI;
import com.bashalex.tossup.R;

import java.io.InputStream;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManagerFactory;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Alex Bash on 27.04.16.
 */
public class APIClient {
    private static RequestsAPI APIInterface;
    private static String url;

    public static class NullHostNameVerifier implements HostnameVerifier {
        @Override
        public boolean verify(String hostname, SSLSession session) {
            Log.d("debug", "hostname: " + hostname);
            return hostname.equals(url);
        }

    }

    @SuppressWarnings("null")
    public static RequestsAPI getAPIClient(Context context) {
        if (APIInterface == null) {
            url = context.getString(R.string.server);
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(String.format(Locale.US, "https://%s/", url))
                    .client(getOkHttpClient(context))
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build();
            APIInterface = retrofit.create(RequestsAPI.class);
        }
        return APIInterface;
    }

    private static OkHttpClient getOkHttpClient(Context context) {
        SSLContext sslConfig = getSSLConfig(context);
        if (sslConfig == null) {
            return null;
        }
        return new OkHttpClient.Builder()
                .hostnameVerifier(new NullHostNameVerifier())
                .sslSocketFactory(sslConfig.getSocketFactory())
                .addInterceptor(new HttpLoggingInterceptor())
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .build();
    }


    private static SSLContext getSSLConfig(Context context)  {
        // Loading CAs from an InputStream
        try {
            CertificateFactory cf;
            cf = CertificateFactory.getInstance("X.509");

            Certificate ca;
            InputStream cert = null;
            try {
                cert = context.getResources().openRawResource(R.raw.server);
                ca = cf.generateCertificate(cert);
            } finally {
                if (cert != null) cert.close();
            }

//            TODO: load real CA
            // Creating a KeyStore containing our trusted CAs
            String keyStoreType = KeyStore.getDefaultType();
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(null, null);
            keyStore.setCertificateEntry("ca", ca);

            // Creating a TrustManager that trusts the CAs in our KeyStore.
            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(keyStore);

            // Creating an SSLSocketFactory that uses our TrustManager
            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, tmf.getTrustManagers(), null);
            return sslContext;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
