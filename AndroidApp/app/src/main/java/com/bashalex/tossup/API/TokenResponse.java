package com.bashalex.tossup.API;

/**
 * Created by Alex Bash on 26.04.16.
 */
public class TokenResponse {
    private String token;

    public TokenResponse(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}
