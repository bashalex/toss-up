package com.bashalex.tossup.Custom;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bashalex.tossup.R;

import butterknife.Bind;
import nucleus.view.NucleusAppCompatActivity;

/**
 * Created by Alex Bash on 01.03.16.
 */
public class TossUpActivity<T extends TossUpPresenter> extends NucleusAppCompatActivity<T> {

    private String TAG;
    private RelativeLayout mainView;
    private boolean progressVisible = false;
    @Bind(R.id.main_layout) RelativeLayout mainLayout;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TAG = getClass().getSimpleName();
        getPresenter().init(this);
        printLog("onCreate()");
    }

    public void showToast(String message) {
        runOnUiThread(() -> {
            if (message != null) {
                Toast.makeText(TossUpActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void printLog(String message) {
        if (message == null) {
            Log.e(TAG, "attempt to print null message");
        } else {
            Log.d(TAG, message);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        printLog("onDestroy()");
    }

    public void showProgress() {
        if (progressVisible) return;
        mainView = new RelativeLayout(this);
        mainView.setBackgroundColor(ContextCompat.getColor(this, R.color.transparent));
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mainView.setLayoutParams(params);
        ProgressBar progressBar = new ProgressBar(this, null, android.R.attr.progressBarStyleLarge);
        RelativeLayout.LayoutParams progressParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        progressParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        progressParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        progressBar.setLayoutParams(progressParams);
        progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.MULTIPLY);
        mainView.addView(progressBar);
        runOnUiThread(() -> {
            if (mainLayout != null) {
                printLog("showProgress()");
                mainLayout.addView(mainView);
                mainLayout.setEnabled(false);
                progressVisible = true;
            }
        });
    }

    public void dismissProgress() {
        if (!progressVisible) return;
        runOnUiThread(() -> {
            if (mainLayout != null) {
                printLog("dismissProgress()");
                mainLayout.removeView(mainView);
                mainLayout.setEnabled(true);
                progressVisible = false;
            }
        });
    }
}
