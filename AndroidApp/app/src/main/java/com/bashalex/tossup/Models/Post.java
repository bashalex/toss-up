package com.bashalex.tossup.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Alex Bash on 18.02.16.
 */
public class Post implements Parcelable {
    public static final Parcelable.Creator CREATOR
            = new Parcelable.Creator() {
        public Post createFromParcel(Parcel in) {
            return new Post(in);
        }
        public Post[] newArray(int size) {
            return new Post[size];
        }
    };
    private Author author;
    private Card left_card, right_card;
    private String description;
    private String id;

    public Post(Card leftCard, Card rightCard, String description, Author author, String id) {
        this.left_card = leftCard;
        this.right_card = rightCard;
        this.description = description;
        this.author = author;
        this.id = id;
    }

    private Post(Parcel in) {
        description = in.readString();
        id = in.readString();
        author = in.readParcelable(Author.class.getClassLoader());
        left_card = in.readParcelable(Card.class.getClassLoader());
        right_card = in.readParcelable(Card.class.getClassLoader());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Card getLeftCard() {
        return left_card;
    }

    public void setLeftCard(Card leftCard) {
        this.left_card = leftCard;
    }

    public Card getRightCard() {
        return right_card;
    }

    public void setRightCard(Card rightCard) {
        this.right_card = rightCard;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "Post{" +
                "leftCard=" + left_card +
                ", rightCard=" + right_card +
                ", description='" + description + '\'' +
                ", author=" + author +
                '}';
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(description);
        out.writeString(id);
        out.writeParcelable(author, flags);
        out.writeParcelable(left_card, flags);
        out.writeParcelable(right_card, flags);
    }
}
