package com.bashalex.tossup.Presenters;

import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Surface;

import com.bashalex.tossup.Custom.TossUpActivity;
import com.bashalex.tossup.Interfaces.CameraInterface;
import com.bashalex.tossup.Interfaces.PreviewStateListener;
import com.bashalex.tossup.Models.CameraModel;

import java.io.IOException;

/**
 * Created by Alex Bash on 02.03.16.
 */
@SuppressWarnings("deprecation")
public class PresenterCameraOld implements CameraInterface {

    private TossUpActivity mActivity;
    private Camera mCamera;
    private int mCameraId;
    private PreviewStateListener previewStateListener;
    private Point mPreviewSize;
    private int currentCamera;
    private Uri imageUri;

    public PresenterCameraOld(TossUpActivity activity) {
        this.mActivity = activity;
        if (activity instanceof PreviewStateListener) {
            this.previewStateListener = (PreviewStateListener) activity;
        } else {
            throw new RuntimeException(activity.toString()
                    + " must implement PreviewStateListener");
        }
    }

    @Override
    public void openCamera(int width, int height, int currentCamera) {
        Log.d("CameraOld", "openCamera");
        mPreviewSize = new Point(width, height);
        this.currentCamera = currentCamera;
        setUpCameraOutputs(currentCamera);
        configureTransform(width, height);
        if (mCamera == null) {
            mActivity.showToast("Something went wrong");
            return;
        }
        SurfaceTexture texture = previewStateListener.onStartPreview();
        try {
            setCameraDisplayOrientation();
            mCamera.setPreviewTexture(texture);
            mCamera.startPreview();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void switchCamera() {
        Log.d("CameraOld", "switchCamera");
        closeCamera();
        openCamera(mPreviewSize.x, mPreviewSize.y, currentCamera == CameraModel.CAMERA_FRONT ? CameraModel.CAMERA_BACK : CameraModel.CAMERA_FRONT);

    }

    @Override
    public void closeCamera() {
        if (mCamera != null) {
            mCamera.stopPreview();
            try {
                mCamera.setPreviewCallback(null);
                mCamera.setErrorCallback(null);
            } catch (Exception e) {
                e.printStackTrace();
            }
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    public void takePicture() {
        if (mCamera == null) return;
        mCamera.takePicture(null, null, pictureCallback);
    }

    @Override
    public void configureTransform(int viewWidth, int viewHeight) {
        Log.d("CameraOld", "configureTransform");
        int rotation = mActivity.getWindowManager().getDefaultDisplay().getRotation();
        Matrix matrix = new Matrix();
        RectF viewRect = new RectF(0, 0, viewWidth, viewHeight);
        RectF bufferRect = new RectF(0, 0, mPreviewSize.y, mPreviewSize.x);
        float centerX = viewRect.centerX();
        float centerY = viewRect.centerY();
        if (Surface.ROTATION_90 == rotation || Surface.ROTATION_270 == rotation) {
            bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY());
            matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL);
            float scale = Math.max(
                    (float) viewHeight / mPreviewSize.y,
                    (float) viewWidth / mPreviewSize.x);
            matrix.postScale(scale, scale, centerX, centerY);
            matrix.postRotate(90 * (rotation - 2), centerX, centerY);
        } else if (Surface.ROTATION_180 == rotation) {
            matrix.postRotate(180, centerX, centerY);
        }
        previewStateListener.onOrientationChanged(matrix);
    }

     public void setCameraDisplayOrientation() {
         Camera.CameraInfo info = new Camera.CameraInfo();
         Camera.getCameraInfo(mCameraId, info);
         int rotation = mActivity.getWindowManager().getDefaultDisplay().getRotation();
         int degrees = 0;
         switch (rotation) {
             case Surface.ROTATION_0: degrees = 0; break;
             case Surface.ROTATION_90: degrees = 90; break;
             case Surface.ROTATION_180: degrees = 180; break;
             case Surface.ROTATION_270: degrees = 270; break;
         }

         int result;
         if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
             result = (info.orientation + degrees) % 360;
             result = (360 - result) % 360;  // compensate the mirror
         } else {  // back-facing
             result = (info.orientation - degrees + 360) % 360;
         }
         mCamera.setDisplayOrientation(result);
     }

    private void setUpCameraOutputs(int currentCamera) {
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        int cameraCount = Camera.getNumberOfCameras();
        for (int camId = 0; camId < cameraCount; ++camId) {
            Camera.getCameraInfo( camId, cameraInfo );
            if ((currentCamera == CameraModel.CAMERA_FRONT &&
                    cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) ||
                (currentCamera == CameraModel.CAMERA_BACK &&
                    cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK)) {
                try {
                    mCameraId = camId;
                    mCamera = Camera.open(camId);
                } catch (RuntimeException e) {
                    Log.e("ex", "CameraModel failed to open: " + e.getLocalizedMessage());
                }
            }
        }
        if (mCamera == null) return;
        Camera.Parameters params = mCamera.getParameters();
        if (params.getSupportedFocusModes().contains(
                Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
            params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        }
        mCamera.setParameters(params);

        Camera.Size previewSize = mCamera.getParameters().getPreviewSize();
        float aspect = (float) previewSize.width / previewSize.height;
        mPreviewSize.x = (int) (mPreviewSize.y / aspect);
        previewStateListener.onRatioChanged(mPreviewSize);
    }

    Camera.PictureCallback pictureCallback = (bytes, camera) -> new AsyncTask<Void, Void, Void>() {
        @Override
        protected Void doInBackground(Void... voids) {
            imageUri = CameraModel.saveImageFromOldCamera(bytes, mActivity,
                    currentCamera == CameraModel.CAMERA_FRONT);
            return null;
        }

        @Override
        protected void onPostExecute(Void path) {
            previewStateListener.onImageSaved(imageUri);
        }
    }.execute();

    @Override
    public void onDestroy() {
        closeCamera();
        mActivity = null;
    }
}
