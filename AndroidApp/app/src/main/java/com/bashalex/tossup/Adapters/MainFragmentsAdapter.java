package com.bashalex.tossup.Adapters;

import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

import com.bashalex.tossup.API.DefaultResponse;
import com.bashalex.tossup.Activities.MainActivity;
import com.bashalex.tossup.Fragments.FragmentActivate;
import com.bashalex.tossup.Fragments.FragmentFeed;
import com.bashalex.tossup.Fragments.FragmentLogin;
import com.bashalex.tossup.Fragments.FragmentProfile;
import com.bashalex.tossup.Fragments.FragmentRegistration;
import com.bashalex.tossup.Fragments.FragmentUpload;
import com.bashalex.tossup.Models.Authorisation;
import com.bashalex.tossup.Models.Post;
import com.bashalex.tossup.Models.ServerRequests;

import rx.Subscription;
import rx.observables.ConnectableObservable;

public class MainFragmentsAdapter extends FragmentPagerAdapter {
    private final static String TAG = "MainFragmentAdapter";
    private final int PAGE_COUNT = 3;
    private MainActivity context;
    private FragmentFeed currentPostInFeed;
    private Post currentPost;
    private Fragment currentThirdFragment;
    private FragmentManager mFragmentManager;
    private Subscription subscr, subscr1, subscr2;

    public MainFragmentsAdapter(FragmentManager fm, MainActivity context, Post post) {
        super(fm);
        Log.d(TAG, "MainFragmentsAdapter()");
        this.mFragmentManager = fm;
        this.context = context;
        this.currentPost = post;
    }

    public void likeAndSwitchToNextPost(boolean isLeft) {
        Log.d(TAG, "switch to the next post");
        System.gc();
        replacePostWithAnimation(isLeft);
    }

    public void updateCurrentPost(Post post) {
        this.currentPost = post;
        Log.d(TAG, "update current post");
        FragmentTransaction ft = mFragmentManager.beginTransaction();
        ft.remove(currentPostInFeed);
        ft.commit();
        updateCurrentPostInFeed();
    }

    public void switchToNextRegistrationStep() {
        Log.d(TAG, "switch to the next registration step");
        FragmentTransaction ft = mFragmentManager.beginTransaction();
        ft.remove(currentThirdFragment);
        ft.commit();
        int status = Authorisation.getStatus(context.getApplicationContext());
        Log.d(TAG, "status: " + status);
        switch (status) {
            case Authorisation.statusLoggedIn:
                currentThirdFragment = FragmentProfile.newInstance();
                break;
            case Authorisation.statusRegistrationStarted:
                currentThirdFragment = FragmentRegistration.newInstance();
                break;
            case Authorisation.statusRequestCodeSent:
                currentThirdFragment = FragmentActivate.newInstance();
                break;
            default:
                currentThirdFragment = FragmentLogin.newInstance();
                break;
        }
        notifyDataSetChanged();
    }

    private void updateCurrentPostInFeed() {
        System.gc();
        if (currentPost == null) {
            Log.d("currentPost", "is null");
            context.showProgress();
            ConnectableObservable<DefaultResponse<Post>> connection =
                    ServerRequests.getNextPost(context, Authorisation.getToken(context));
            if (subscr != null && !subscr.isUnsubscribed()) subscr.unsubscribe();
            subscr = connection.filter(response -> response.getErrorCode() == 0)
                    .subscribe(response -> {
                        currentPost = response.getData();
                    });
            connection.connect();
        }
        currentPostInFeed = FragmentFeed.newInstance(currentPost);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        Log.d(TAG, "getItem(" + position + ")");
        switch (position) {
            case 0:
                if (currentPostInFeed == null) {
                    updateCurrentPostInFeed();
                }
                return currentPostInFeed;
            case 1:
                return FragmentUpload.newInstance();
            case 2:
                if (currentThirdFragment == null) {
                    int status = Authorisation.getStatus(context.getApplicationContext());
                    switch (status) {
                        case Authorisation.statusLoggedIn:
                            currentThirdFragment = FragmentProfile.newInstance();
                            break;
                        case Authorisation.statusRegistrationStarted:
                            currentThirdFragment = FragmentRegistration.newInstance();
                            break;
                        case Authorisation.statusRequestCodeSent:
                            currentThirdFragment = FragmentActivate.newInstance();
                            break;
                        default:
                            currentThirdFragment = FragmentLogin.newInstance();
                            break;
                    }
                }
                return currentThirdFragment;
        }
        return null;
    }

    @Override
    public int getItemPosition(Object object) {
        Log.d(TAG, "object: " + object.getClass());
        if (object instanceof FragmentFeed && !object.equals(currentPostInFeed)) {
            Log.d(TAG, "position None");
            return POSITION_NONE;
        }
        if ((object instanceof FragmentLogin || object instanceof FragmentRegistration ||
                object instanceof FragmentActivate || object instanceof FragmentProfile)
                && !object.equals(currentThirdFragment)) {
            Log.d(TAG, "position None");
            return POSITION_NONE;
        }
        Log.d(TAG, "position Unchanged");
        return POSITION_UNCHANGED;
    }

    private void replacePostWithAnimation(boolean isLeft) {
        AlphaAnimation animation = new AlphaAnimation(1.0f, 0);
        animation.setDuration(300);

        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override public void onAnimationStart(Animation animation) { }

            @Override
            public void onAnimationEnd(Animation animation) {
                context.showProgress();
                ConnectableObservable<DefaultResponse<Post>> connection =
                        ServerRequests.likeAndGetNextPost(context,
                                Authorisation.getToken(context), currentPost.getId(), isLeft);
                if (subscr1 != null && !subscr1.isUnsubscribed()) subscr1.unsubscribe();
                subscr1 = connection.filter(response -> response.getErrorCode() == 500)
                        .subscribe(response -> {
                            context.onLikeWithoutRegistration();
                        });
                if (subscr2 != null && !subscr2.isUnsubscribed()) subscr2.unsubscribe();
                subscr2 = connection.filter(response -> response.getErrorCode() == 0)
                        .subscribe(response -> {
//                            remove old post
                            FragmentTransaction ft = mFragmentManager.beginTransaction();
                            ft.remove(currentPostInFeed);
                            ft.commit();
//                            set the new one
                            currentPost = response.getData();
                            Log.d("nextPost", currentPost.toString());
                            currentPostInFeed = FragmentFeed.newInstance(currentPost);
                            notifyDataSetChanged();
                        });
                connection.connect();
            }

            @Override public void onAnimationRepeat(Animation animation) { }
        });

        currentPostInFeed.mainView.startAnimation(animation);
    }
}