package com.bashalex.tossup.Activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;

import com.bashalex.tossup.API.DefaultResponse;
import com.bashalex.tossup.Adapters.MainFragmentsAdapter;
import com.bashalex.tossup.Custom.TossUpActivity;
import com.bashalex.tossup.Interfaces.PostChangeListener;
import com.bashalex.tossup.Interfaces.RegistrationRedirectListener;
import com.bashalex.tossup.Interfaces.RegistrationStepChangeListener;
import com.bashalex.tossup.Models.Authorisation;
import com.bashalex.tossup.Models.Post;
import com.bashalex.tossup.Models.ServerRequests;
import com.bashalex.tossup.Models.TossUpCodes;
import com.bashalex.tossup.Presenters.PresenterMain;
import com.bashalex.tossup.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import nucleus.factory.RequiresPresenter;
import rx.Subscription;
import rx.observables.ConnectableObservable;

@RequiresPresenter(PresenterMain.class)
public class MainActivity extends TossUpActivity<PresenterMain>
        implements PostChangeListener, RegistrationStepChangeListener,
                   RegistrationRedirectListener {

    private Subscription subscr;

    private MainFragmentsAdapter mainFragmentsAdapter;
    @Bind(R.id.toolbar) Toolbar toolbar;
    @Bind(R.id.viewpager) ViewPager viewPager;
    @Bind(R.id.sliding_tabs) TabLayout tabLayout;


    private final int[] imageResId = {
            R.drawable.ic_favorite_border_black_24dp,
            R.drawable.ic_cloud_upload_black_24dp,
            R.drawable.ic_perm_identity_black_24dp
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        init();
        testRequestWritePermission();
    }

//    TODO: remove this function before production
    public void testRequestWritePermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                showToast(getString(R.string.request_gallery_permissions));
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 177);
            }
        } else {
            printLog("permission granted");
        }
    }

    private void init() {
        setupTabs();
        update();
    }

    private void setupTabs() {
        mainFragmentsAdapter = new MainFragmentsAdapter(
                getSupportFragmentManager(), this, null);
        viewPager.setAdapter(mainFragmentsAdapter);
        tabLayout.setupWithViewPager(viewPager);
        for (int i = 0; i < 3; ++i) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            if (tab != null) tab.setIcon(imageResId[i]);
        }
    }

    public void update() {
        showProgress();
        ConnectableObservable<DefaultResponse<Post>> connection =
                ServerRequests.getNextPost(this, Authorisation.getToken(this));
        if (subscr != null && !subscr.isUnsubscribed()) subscr.unsubscribe();
        subscr = connection.filter(response -> response.getErrorCode() == 0)
                .subscribe(response -> {
                    printLog("response: " + response.toString());
                    mainFragmentsAdapter.updateCurrentPost(response.getData());
                });
        connection.connect();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onLikePost(boolean isLeft) {
        mainFragmentsAdapter.likeAndSwitchToNextPost(isLeft);
    }

    @Override
    public void onNextStep() {
        mainFragmentsAdapter.switchToNextRegistrationStep();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onLikeWithoutRegistration() {
        viewPager.setCurrentItem(2);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case TossUpCodes.getGalleryPermissions:
                if (!(grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    showToast(getString(R.string.gallery_no_permission));
                }
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (!subscr.isUnsubscribed()) subscr.unsubscribe();
    }
}
