package com.bashalex.tossup.Interfaces;

/**
 * Created by Alex Bash on 19.02.16.
 */
public interface CommentsUpdatedListener {
    void onCommentsUpdated();
}
