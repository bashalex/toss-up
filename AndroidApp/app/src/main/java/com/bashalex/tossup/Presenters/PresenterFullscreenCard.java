package com.bashalex.tossup.Presenters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bashalex.tossup.Adapters.CommentsAdapter;
import com.bashalex.tossup.Custom.TossUpPresenter;
import com.bashalex.tossup.Interfaces.CommentClickListener;
import com.bashalex.tossup.Interfaces.CommentsUpdatedListener;
import com.bashalex.tossup.Models.Post;
import com.bashalex.tossup.Models.ServerRequests;

/**
 * Created by Alex Bash on 18.02.16.
 */
public class PresenterFullscreenCard extends TossUpPresenter {

    private Post mPost;
    private boolean left;

    private CommentsUpdatedListener commentsUpdatedListener;
    private CommentClickListener commentClickListener;

    private CommentsAdapter adapter;


    public void init(Context context, Post post, boolean left, RecyclerView view) {
        this.mPost = post;
        this.left = left;
        initListeners(context);
        updateComments(view);
    }

    public void updateComments(RecyclerView view) {
        if (null == adapter) {
            setAdapter(view);
        } else {
            adapter.mComments = ServerRequests.getComments(mPost, left);
            adapter.notifyDataSetChanged();
        }
        commentsUpdatedListener.onCommentsUpdated();
    }

    private void setAdapter(View view) {
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);
            recyclerView.setLayoutManager(mLayoutManager);
            adapter = new CommentsAdapter(context, commentClickListener, ServerRequests.getComments(mPost, left));
            recyclerView.setAdapter(adapter);
        }
    }

    private void initListeners(Context context) {
        if (context instanceof CommentClickListener) {
            commentClickListener = (CommentClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement CommentClickListener");
        }
        if (context instanceof CommentsUpdatedListener) {
            commentsUpdatedListener = (CommentsUpdatedListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement CommentsUpdatedListener");
        }
    }

    private void removeListeners() {
        commentsUpdatedListener = null;
        commentClickListener = null;
    }

    public void onDestroy() {
        super.onDestroy();
        removeListeners();
        adapter = null;
    }
}
