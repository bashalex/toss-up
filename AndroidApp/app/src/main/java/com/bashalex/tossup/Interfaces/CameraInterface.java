package com.bashalex.tossup.Interfaces;

/**
 * Created by Alex Bash on 02.03.16.
 */
public interface CameraInterface {
    void openCamera(int width, int height, int currentCamera);
    void switchCamera();
    void closeCamera();
    void takePicture();
    void configureTransform(int width, int height);
    void onDestroy();
}
