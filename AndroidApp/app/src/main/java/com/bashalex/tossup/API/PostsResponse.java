package com.bashalex.tossup.API;

import com.bashalex.tossup.Models.Post;

import java.util.List;

/**
 * Created by Alex Bash on 14.06.16.
 */
public class PostsResponse {
    private List<Post> posts;

    public PostsResponse(List<Post> posts) {
        this.posts = posts;
    }

    public List<Post> getPosts() {
        return posts;
    }

    @Override
    public String toString() {
        return "PostsResponse{" +
                "posts=" + posts +
                '}';
    }
}
