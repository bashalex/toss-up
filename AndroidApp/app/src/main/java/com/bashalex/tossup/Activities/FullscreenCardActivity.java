package com.bashalex.tossup.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.bashalex.tossup.Custom.TossUpActivity;
import com.bashalex.tossup.Interfaces.CommentClickListener;
import com.bashalex.tossup.Interfaces.CommentsUpdatedListener;
import com.bashalex.tossup.Models.Card;
import com.bashalex.tossup.Models.Comment;
import com.bashalex.tossup.Models.Post;
import com.bashalex.tossup.Presenters.PresenterFullscreenCard;
import com.bashalex.tossup.R;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import nucleus.factory.RequiresPresenter;

@RequiresPresenter(PresenterFullscreenCard.class)
public class FullscreenCardActivity extends TossUpActivity<PresenterFullscreenCard>
        implements CommentClickListener, CommentsUpdatedListener {

    @Bind(R.id.header_image) ImageView headerImage;
    @Bind(R.id.comments_list) RecyclerView commentsView;
    @Bind(R.id.comments_refresh_layout) SwipeRefreshLayout refreshLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen_card);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        Post currentPost = intent.getParcelableExtra(getString(R.string.intentArg1));
        boolean left = intent.getBooleanExtra(getString(R.string.intentArg2), true);
        Card currentCard = left ? currentPost.getLeftCard() : currentPost.getRightCard();
        getPresenter().init(this, currentPost, left, commentsView);
        Picasso.with(this)
                .load(currentCard.getImageLink())
                .into(headerImage);
        refreshLayout.setOnRefreshListener(() -> getPresenter().updateComments(commentsView));
    }


    @Override
    public void onCommentClick(Comment comment, View view) {

    }

    @Override
    public void onCommentsUpdated() {
        refreshLayout.setRefreshing(false);
    }
}
