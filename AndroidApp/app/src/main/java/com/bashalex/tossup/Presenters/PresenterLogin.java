package com.bashalex.tossup.Presenters;

import com.bashalex.tossup.Custom.TossUpPresenter;
import com.bashalex.tossup.Interfaces.RegistrationStepChangeListener;
import com.bashalex.tossup.Models.Authorisation;

/**
 * Created by Alex Bash on 21.02.16.
 */
public class PresenterLogin extends TossUpPresenter {

    public void login(String login, String password) {
        if (mActivity instanceof RegistrationStepChangeListener) {
            Authorisation.login(mActivity, login, password, (RegistrationStepChangeListener) mActivity);
        } else {
            throw new RuntimeException(mActivity.toString()
                    + " must implement RegistrationStepChangeListener");
        }
    }

    public void openRegistrationScreen() {
        if (mActivity instanceof RegistrationStepChangeListener) {
            Authorisation.startRegistration(mActivity, (RegistrationStepChangeListener) mActivity);
        } else {
            throw new RuntimeException(mActivity.toString()
                    + " must implement RegistrationStepChangeListener");
        }
    }
}
