package com.bashalex.tossup.API;

/**
 * Created by Alex Bash on 27.04.16.
 */
public class DefaultResponse<T> {
    private T data;
    private String status;
    private int errorCode;
    private String message;

    public DefaultResponse(T data, String status, int errorCode, String message) {
        this.data = data;
        this.status = status;
        this.errorCode = errorCode;
        this.message = message;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public T getData() {
        return data;
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "DefaultResponse{" +
                "data=" + data +
                ", status='" + status + '\'' +
                ", errorCode=" + errorCode +
                ", message='" + message + '\'' +
                '}';
    }
}