package com.bashalex.tossup.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.bashalex.tossup.Custom.TossUpFragment;
import com.bashalex.tossup.Models.TossUpCodes;
import com.bashalex.tossup.Presenters.PresenterRegistration;
import com.bashalex.tossup.R;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import nucleus.factory.RequiresPresenter;

@RequiresPresenter(PresenterRegistration.class)
public class FragmentRegistration extends TossUpFragment<PresenterRegistration> {

    @Bind(R.id.register_email) EditText emailField;
    @Bind(R.id.register_login) EditText loginField;
    @Bind(R.id.register_password) EditText passwordField;
    @Bind(R.id.register_photo) CircleImageView registerPhoto;

    private Uri avatar;

    public FragmentRegistration() {
        // Required empty public constructor
    }

    public static FragmentRegistration newInstance() {
        return new FragmentRegistration();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            avatar = savedInstanceState.getParcelable("uri");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_registration, container, false);
        ButterKnife.bind(this, view);
        return view;
    }
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putParcelable("uri", avatar);
        super.onSaveInstanceState(bundle);
    }


    @OnClick(R.id.register_button)
    public void register() {
        String email = emailField.getText().toString();
        String nickname = loginField.getText().toString();
        String password = passwordField.getText().toString();
        printLog(email);
        String error = getPresenter().validate(email, nickname, password);
        if (null == error) {
            getPresenter().register(email, nickname, password, avatar);
        } else {
            showToast(error);
        }
    }

    @OnClick(R.id.back_button)
    public void back() {
        getPresenter().breakRegistration();
    }

    @OnClick(R.id.register_photo)
    public void loadPhoto(View view) {
        showCameraDialog(view);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == TossUpCodes.avatarCamera ||
                    requestCode == TossUpCodes.avatarGallery) {
                avatar = data.getData();
                Picasso.with(getActivity())
                        .load(avatar)
                        .into(registerPhoto);
            }
        } else {
            showToast(getString(R.string.choose_image));
        }
    }
}