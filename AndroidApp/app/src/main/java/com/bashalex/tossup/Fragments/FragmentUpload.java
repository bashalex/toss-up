package com.bashalex.tossup.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.bashalex.tossup.Custom.TossUpFragment;
import com.bashalex.tossup.Interfaces.QuestionUploadedListener;
import com.bashalex.tossup.Models.TossUpCodes;
import com.bashalex.tossup.Models.Utils;
import com.bashalex.tossup.Presenters.PresenterUpload;
import com.bashalex.tossup.R;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import nucleus.factory.RequiresPresenter;

@RequiresPresenter(PresenterUpload.class)
public class FragmentUpload extends TossUpFragment<PresenterUpload>
        implements QuestionUploadedListener {

    public FragmentUpload() { }

    @Bind(R.id.left_card_image) ImageView leftCardImage;
    @Bind(R.id.right_card_image) ImageView rightCardImage;
    @Bind(R.id.left_card) CardView leftCard;
    @Bind(R.id.right_card) CardView rightCard;
    @Bind(R.id.left_card_hint) ImageView leftCardHint;
    @Bind(R.id.right_card_hint) ImageView rightCardHint;
    @Bind(R.id.left_card_description) EditText leftCardDescription;
    @Bind(R.id.right_card_description) EditText rightCardDescription;
    @Bind(R.id.question_text) EditText question;

    private Uri leftImage, rightImage;

    public static FragmentUpload newInstance() {
        return new FragmentUpload();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            leftImage = savedInstanceState.getParcelable("uri1");
            rightImage = savedInstanceState.getParcelable("uri2");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        printLog("FRAGMENT_UPLOAD ON_CREATE_VIEW");
        View view = inflater.inflate(R.layout.fragment_upload, container, false);
        ButterKnife.bind(this, view);
        setElementSizes();
        return view;
    }

    private void setElementSizes() {
        Point size = Utils.computeSizeForCards(getActivity());
        applySizes(leftCard, size);
        applySizes(rightCard, size);
    }

    private void applySizes(View card, Point size) {
        ViewGroup.LayoutParams params = card.getLayoutParams();
        params.width = size.x;
        params.height = size.y;
        card.setLayoutParams(params);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getPresenter().setListeners(this);
    }

    @OnClick({R.id.left_card, R.id.right_card})
    public void onCardClick(View v) {
        showCameraDialog(v);
    }

    @OnClick(R.id.upload_button)
    public void upload() {
        getPresenter().upload(leftCardDescription, rightCardDescription, question, leftImage, rightImage);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            System.gc();
            if (requestCode == TossUpCodes.leftCardCamera ||
                    requestCode == TossUpCodes.leftCardGallery) {
                leftImage = data.getData();
                Picasso.with(getActivity())
                        .load(leftImage)
                        .resize(800, 800)
                        .centerCrop()
                        .placeholder(R.drawable.prbar)
                        .error(R.drawable.err)
                        .into(leftCardImage);
                leftCardHint.setVisibility(View.GONE);
            } else if (requestCode == TossUpCodes.rightCardCamera ||
                    requestCode == TossUpCodes.rightCardGallery) {
                rightImage = data.getData();
                Picasso.with(getActivity())
                        .load(rightImage)
                        .resize(800, 800)
                        .centerCrop()
                        .placeholder(R.drawable.prbar)
                        .error(R.drawable.err)
                        .into(rightCardImage);
                rightCardHint.setVisibility(View.GONE);
            }
        } else {
            showToast(getString(R.string.choose_image));
        }
    }

    public void onSaveInstanceState(Bundle bundle) {
        bundle.putParcelable("uri1", leftImage);
        bundle.putParcelable("uri2", rightImage);
        super.onSaveInstanceState(bundle);
    }

    @Override
    public void onQuestionUploaded() {
        leftCardImage.setImageDrawable(null);
        rightCardImage.setImageDrawable(null);
        leftCardHint.setVisibility(View.VISIBLE);
        rightCardHint.setVisibility(View.VISIBLE);
        leftCardDescription.setText("");
        rightCardDescription.setText("");
        question.setText("");
    }
}
