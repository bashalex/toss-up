package com.bashalex.tossup.Presenters;

import android.net.Uri;
import android.util.Patterns;

import com.bashalex.tossup.Custom.TossUpPresenter;
import com.bashalex.tossup.Interfaces.RegistrationStepChangeListener;
import com.bashalex.tossup.Models.Authorisation;
import com.bashalex.tossup.R;

/**
 * Created by bashalex on 22.02.16.
 */
public class PresenterRegistration extends TossUpPresenter {

    public String validate(String email, String nickname, String password) {
        if (email.isEmpty()) {
            return mActivity.getString(R.string.empty_email);
        }
        if (!email.matches(Patterns.EMAIL_ADDRESS.toString())) {
            return mActivity.getString(R.string.invalid_email);
        }
        if (nickname.isEmpty()) {
            return mActivity.getString(R.string.empty_nickname);
        }
        if (password.isEmpty()) {
            return mActivity.getString(R.string.empty_password);
        }
        if (password.length() < 8) {
            return mActivity.getString(R.string.short_password);
        }
        if (!password.matches(".*\\d.*")) {
            return mActivity.getString(R.string.no_num_password);
        }
        if (password.matches("[0-9]+")) {
            return mActivity.getString(R.string.only_nums_password);
        }
        return null;
    }

    public void register(String email, String nickname, String password, Uri avatar) {
        if (mActivity instanceof RegistrationStepChangeListener) {
            Authorisation.register(mActivity, email, nickname, password, avatar, (RegistrationStepChangeListener) mActivity);
        } else {
            throw new RuntimeException(mActivity.toString()
                    + " must implement RegistrationStepChangeListener");
        }
    }

    public void breakRegistration() {
        if (mActivity instanceof RegistrationStepChangeListener) {
            Authorisation.breakRegistration(mActivity, (RegistrationStepChangeListener) mActivity);
        } else {
            throw new RuntimeException(mActivity.toString()
                    + " must implement RegistrationStepChangeListener");
        }
    }
}