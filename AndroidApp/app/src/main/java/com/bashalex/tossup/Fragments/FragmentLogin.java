package com.bashalex.tossup.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.bashalex.tossup.Custom.TossUpFragment;
import com.bashalex.tossup.Presenters.PresenterLogin;
import com.bashalex.tossup.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import nucleus.factory.RequiresPresenter;

@RequiresPresenter(PresenterLogin.class)
public class FragmentLogin extends TossUpFragment<PresenterLogin> {

    @Bind(R.id.login) EditText loginField;
    @Bind(R.id.password) EditText passwordField;

    public FragmentLogin() { }

    public static FragmentLogin newInstance() {
        return new FragmentLogin();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.login_button)
    public void login() {
        String login = loginField.getText().toString();
        String password = passwordField.getText().toString();
        getPresenter().login(login, password);
    }

    @OnClick(R.id.register_button)
    public void register() {
        getPresenter().openRegistrationScreen();
    }

}