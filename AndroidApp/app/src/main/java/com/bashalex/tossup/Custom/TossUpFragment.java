package com.bashalex.tossup.Custom;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.bashalex.tossup.Activities.CameraActivity;
import com.bashalex.tossup.Interfaces.ActivityRequestedListener;

import butterknife.ButterKnife;
import nucleus.view.NucleusSupportFragment;

/**
 * Created by Alex Bash on 21.02.16.
 */
public class TossUpFragment<P extends TossUpPresenter> extends NucleusSupportFragment<P>
        implements ActivityRequestedListener {

    private String TAG;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TAG = getClass().getSimpleName();
        printLog("onCreate()");
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getPresenter().init((TossUpActivity) getActivity());
    }

    public void showToast(String message) {
        getActivity().runOnUiThread(() -> {
            if (message != null) {
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void printLog(String message) {
        if (message == null) {
            Log.e(TAG, "attempt to print null message");
        } else {
            Log.d(TAG, message);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        printLog("onDestroy()");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    public void showCameraDialog(View view) {
        getPresenter().showCameraDialog(view, this);
    }

    @Override
    public void onGalleryRequested(int code) {
        if (!getPresenter().checkGalleryPermissions()) {
            return;
        }
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_PICK);
        intent.setType("image/*");
        printLog("gallery requested");
        startActivityForResult(Intent.createChooser(intent,
                "Select Picture"), code);
    }

    @Override
    public void onCameraScreenRequested(int code) {
        Intent intent = new Intent(getActivity(), CameraActivity.class);
        startActivityForResult(intent, code);
    }
}
