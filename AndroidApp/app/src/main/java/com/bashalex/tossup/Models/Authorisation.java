package com.bashalex.tossup.Models;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.util.Log;

import com.bashalex.tossup.API.AccountResponse;
import com.bashalex.tossup.API.DefaultResponse;
import com.bashalex.tossup.API.TokenResponse;
import com.bashalex.tossup.Custom.TossUpActivity;
import com.bashalex.tossup.Interfaces.RegistrationStepChangeListener;
import com.bashalex.tossup.R;

import io.realm.Realm;
import io.realm.RealmResults;
import rx.Subscription;
import rx.observables.ConnectableObservable;

/**
 * Created by Alex Bash on 21.02.16.
 */
public class Authorisation {

    private static Subscription subscr, subscr1, subscr2;
    public final static int statusNoToken = 0;
    public final static int statusRegistrationStarted = 1;
    public final static int statusRequestCodeSent = 2;
    public final static int statusTokenValid = 3;
    public final static int statusTokenInvalid = 4;
    public final static int statusLoggedIn = 5;

    public static int getStatus(Context appContext) {
        SharedPreferences spref = PreferenceManager.getDefaultSharedPreferences(appContext);
        String local_status_field = appContext.getResources().getString(R.string.registration_local_status);
        return spref.getInt(local_status_field, statusNoToken);
    }

    public static String getToken(Context appContext) {
        SharedPreferences spref = PreferenceManager.getDefaultSharedPreferences(appContext);
        String token_field = appContext.getResources().getString(R.string.token);
        return spref.getString(token_field, null);
    }

    public static void startRegistration(Context appContext,
                                         RegistrationStepChangeListener listener) {
        SharedPreferences spref = PreferenceManager.getDefaultSharedPreferences(appContext);
        String local_status_field = appContext.getResources().getString(R.string.registration_local_status);
        spref.edit().putInt(local_status_field, statusRegistrationStarted).apply();
        listener.onNextStep();
    }

    public static void breakRegistration(Context appContext,
                                         RegistrationStepChangeListener listener) {
        SharedPreferences spref = PreferenceManager.getDefaultSharedPreferences(appContext);
        String local_status_field = appContext.getResources().getString(R.string.registration_local_status);
        String token_field = appContext.getResources().getString(R.string.token);
        spref.edit().putInt(local_status_field, statusNoToken).apply();
        spref.edit().putString(token_field, null).apply();
        listener.onNextStep();
    }

    public static void register(TossUpActivity appContext, String email,
                                String nickname, String password, Uri image,
                                RegistrationStepChangeListener listener) {
        appContext.showProgress();
        ConnectableObservable<DefaultResponse<TokenResponse>> connection =
                ServerRequests.registerNewAccount(appContext, email, nickname, password, image);
        if (subscr != null && !subscr.isUnsubscribed()) subscr.unsubscribe();
        subscr = connection.filter(response -> response.getErrorCode() == 0)
                .subscribe(response -> {
                    Log.d("response", "token: " + response.getData().getToken());
                    SharedPreferences spref = PreferenceManager.getDefaultSharedPreferences(appContext);
                    String local_status_field = appContext.getResources().getString(R.string.registration_local_status);
                    String token_field = appContext.getResources().getString(R.string.token);
                    spref.edit().putInt(local_status_field, statusRequestCodeSent).apply();
                    spref.edit().putString(token_field, response.getData().getToken()).apply();
                    listener.onNextStep();
                });
        connection.connect();
    }

    public static void validateCode(TossUpActivity appContext, String code,
                                    RegistrationStepChangeListener listener) {
        appContext.showProgress();
        ConnectableObservable<DefaultResponse<Object>> connection =
                ServerRequests.validateCode(appContext, code);
        if (subscr != null && !subscr.isUnsubscribed()) subscr.unsubscribe();
        subscr = connection.filter(response -> response.getErrorCode() == 0)
                .subscribe(response -> {
                    SharedPreferences spref = PreferenceManager.getDefaultSharedPreferences(appContext);
                    String local_status_field = appContext.getResources().getString(R.string.registration_local_status);
                    spref.edit().putInt(local_status_field, statusTokenValid).apply();
                    listener.onNextStep();
                });
        connection.connect();
    }

    public static void login(TossUpActivity appContext, RegistrationStepChangeListener listener) {
        appContext.showProgress();
        SharedPreferences spref = PreferenceManager.getDefaultSharedPreferences(appContext);
        String token_field = appContext.getResources().getString(R.string.token);
        String token = spref.getString(token_field, null);
        if (token == null) return;

        String local_status_field = appContext.getResources().getString(R.string.registration_local_status);
        ConnectableObservable<DefaultResponse<AccountResponse>> connection =
                ServerRequests.login(appContext, token);
        if (subscr1 != null && !subscr1.isUnsubscribed()) subscr1.unsubscribe();
        subscr1 = connection.filter(response -> response.getErrorCode() == 0)
                .subscribe(response -> {
                    spref.edit().putInt(local_status_field, statusLoggedIn).apply();
                    Realm realm = Realm.getDefaultInstance();
                    realm.beginTransaction();
                    realm.copyToRealmOrUpdate(new Account(response.getData()));
                    realm.commitTransaction();
                    realm.close();
                    listener.onNextStep();
                });
        if (subscr2 != null && !subscr2.isUnsubscribed()) subscr2.unsubscribe();
        subscr2 = connection.filter(response -> response.getErrorCode() == 100)
                .subscribe(response -> spref.edit().putInt(local_status_field, statusTokenInvalid).apply());
        connection.connect();
    }

    public static void login(TossUpActivity appContext, String nickname,
                                String password, RegistrationStepChangeListener listener) {
        appContext.showProgress();
        SharedPreferences spref = PreferenceManager.getDefaultSharedPreferences(appContext);
        ConnectableObservable<DefaultResponse<TokenResponse>> connection =
                ServerRequests.getToken(appContext, nickname, password);
        if (subscr != null && !subscr.isUnsubscribed()) subscr.unsubscribe();
        subscr = connection.filter(response -> response.getErrorCode() == 0)
                .subscribe(response -> {
                    String local_status_field = appContext.getResources().getString(R.string.registration_local_status);
                    String token_field = appContext.getResources().getString(R.string.token);
                    spref.edit().putInt(local_status_field, statusTokenValid).apply();
                    spref.edit().putString(token_field, response.getData().getToken()).apply();
                    login(appContext, listener);
                });
        connection.connect();
    }

    public static void logout(Context appContext, RegistrationStepChangeListener listener) {
        SharedPreferences spref = PreferenceManager.getDefaultSharedPreferences(appContext);
        String local_status_field = appContext.getResources().getString(R.string.registration_local_status);
        String token_field = appContext.getResources().getString(R.string.token);
        spref.edit().putInt(local_status_field, statusNoToken).apply();
        spref.edit().putString(token_field, null).apply();
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Account> accounts = realm.where(Account.class).findAll();
        realm.beginTransaction();
        accounts.clear();
        realm.commitTransaction();
        realm.close();
        listener.onNextStep();
    }
}
