package com.bashalex.tossup.Activities;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.SurfaceTexture;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.TextureView;

import com.bashalex.tossup.Custom.TossUpActivity;
import com.bashalex.tossup.Interfaces.PreviewStateListener;
import com.bashalex.tossup.Models.TossUpCodes;
import com.bashalex.tossup.Presenters.PresenterCamera;
import com.bashalex.tossup.R;
import com.bashalex.tossup.Views.AutoFitTextureView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import nucleus.factory.RequiresPresenter;

@RequiresPresenter(PresenterCamera.class)
public class CameraActivity extends TossUpActivity<PresenterCamera>
        implements TextureView.SurfaceTextureListener, PreviewStateListener {

    @Bind(R.id.texture_view) AutoFitTextureView cameraView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        ButterKnife.bind(this);
        getPresenter().checkPermissions();
    }

    @OnClick(R.id.camera_take_picture)
    public void takePicture() {
        showProgress();
        getPresenter().takePicture();
    }

    @OnClick(R.id.camera_switch)
    public void switchCamera() {
        getPresenter().switchCamera();
    }


    public void onResume() {
        super.onResume();
        if (cameraView.isAvailable()) {
            getPresenter().openCamera(cameraView);
        } else {
            cameraView.setSurfaceTextureListener(this);
        }
    }

    public void onPause() {
        super.onPause();
        getPresenter().closeCamera();
    }

    @Override
    public void onOrientationChanged(Matrix matrix) {
        printLog("onOrientationChanged");
        if (null != cameraView) {
            cameraView.setTransform(matrix);
        }
    }

    @Override
    public void onRatioChanged(Point previewSize) {
        // works only for Portrait Orientation
        cameraView.setAspectRatio(previewSize.x, previewSize.y);
    }

    @Override
    public SurfaceTexture onStartPreview() {
        printLog("onStartPreview");
        SurfaceTexture texture = cameraView.getSurfaceTexture();
        texture.setDefaultBufferSize(cameraView.getWidth(), cameraView.getHeight());
        return texture;
    }

    @Override
    public void onImageSaved(Uri imageUri) {
        printLog("onImageSaved");
        dismissProgress();
        Intent data = new Intent();
        data.setData(imageUri);
        if (getParent() == null) {
            setResult(Activity.RESULT_OK, data);
        } else {
            getParent().setResult(Activity.RESULT_OK, data);
        }
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case TossUpCodes.getCameraPermissions:
                if (!(grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    showToast(getString(R.string.camera_no_permission));
                    finish();
                }
        }
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        printLog("onSurfaceTextureAvailable");
        getPresenter().openCamera(cameraView);
    }

    @Override public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
        getPresenter().configureTransform(width, height);
    }

    @Override public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) { return false; }

    @Override public void onSurfaceTextureUpdated(SurfaceTexture surface) { }

}
