package com.bashalex.tossup.Fragments;

import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bashalex.tossup.Activities.MainActivity;
import com.bashalex.tossup.Custom.TossUpFragment;
import com.bashalex.tossup.Models.Post;
import com.bashalex.tossup.Models.Utils;
import com.bashalex.tossup.Presenters.PresenterFeed;
import com.bashalex.tossup.R;
import com.jakewharton.rxbinding.view.RxView;
import com.squareup.picasso.Picasso;

import java.util.concurrent.TimeUnit;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import nucleus.factory.RequiresPresenter;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.observables.ConnectableObservable;

@RequiresPresenter(PresenterFeed.class)
public class FragmentFeed extends TossUpFragment<PresenterFeed> {

    private final static String postArg = "post";
    @Bind(R.id.post) public RelativeLayout mainView;
    @Bind(R.id.no_post_placeholder) RelativeLayout noPostPlaceholder;
    @Bind(R.id.cards_layout) LinearLayout cardsLayout;
    @Bind(R.id.left_card) CardView leftCard;
    @Bind(R.id.right_card) CardView rightCard;
    @Bind(R.id.left_card_image) ImageView leftCardImage;
    @Bind(R.id.right_card_image) ImageView rightCardImage;
    @Bind(R.id.left_like) ImageView leftLikeImage;
    @Bind(R.id.right_like) ImageView rightLikeImage;
    @Bind(R.id.author_info) LinearLayout authorLayout;
    @Bind(R.id.author_image) ImageView authorImage;
    @Bind(R.id.author_nickname) TextView authorNickname;
    @Bind(R.id.question_text) TextView postDescription;
    @Bind(R.id.left_card_description) TextView leftCardDescription;
    @Bind(R.id.right_card_description) TextView rightCardDescription;

    private Post currentPost;
    private Subscription singleClicks, doubleClicks;

    public FragmentFeed() {}

    public static FragmentFeed newInstance(Post post) {
        FragmentFeed fragment = new FragmentFeed();
        Bundle args = new Bundle();
        args.putParcelable(postArg, post);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            currentPost = getArguments().getParcelable(postArg);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        printLog("FRAGMENT_FEED ON_CREATE_VIEW");
        View view = inflater.inflate(R.layout.fragment_feed, container, false);
        ButterKnife.bind(this, view);
        setElementSizes();
        listenCardClicks();
        initViews();
        return view;
    }

    private void setElementSizes() {
        Point size = Utils.computeSizeForCards(getActivity());
        applySizes(leftCard, size);
        applySizes(rightCard, size);
    }

    private void applySizes(View card, Point size) {
        ViewGroup.LayoutParams params = card.getLayoutParams();
        params.width = size.x;
        params.height = size.y;
        card.setLayoutParams(params);
    }

    private void listenCardClicks() {
        ConnectableObservable<Void> allLeftCardClicks = RxView.clicks(leftCard).publish();
        ConnectableObservable<Void> allRightCardClicks = RxView.clicks(rightCard).publish();

        ConnectableObservable<cardClickEvent> cardClicks = allRightCardClicks
                .buffer(allRightCardClicks.debounce(200, TimeUnit.MILLISECONDS))
                .map(clicks -> new cardClickEvent(rightCardImage, rightLikeImage, clicks.size(), false))
                .mergeWith(allLeftCardClicks
                        .buffer(allLeftCardClicks.debounce(200, TimeUnit.MILLISECONDS))
                        .map(clicks -> new cardClickEvent(leftCardImage, leftLikeImage, clicks.size(), true))
                )
                .publish();

        allLeftCardClicks.connect();
        allRightCardClicks.connect();

//        TODO: make comments available
        singleClicks = cardClicks.filter(event -> event.numberOfClicks == 1)
                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(event -> getPresenter().openFullscreenCard(event.cardImage, currentPost, event.isLeft));
                .subscribe();
        doubleClicks = cardClicks.filter(event -> event.numberOfClicks == 2)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(event -> getPresenter().likePost(event.likeImage, event.isLeft));

        cardClicks.connect();
    }

    private void initViews() {
        if (currentPost == null) {
            postDescription.setVisibility(View.GONE);
            cardsLayout.setVisibility(View.GONE);
            authorLayout.setVisibility(View.GONE);
            noPostPlaceholder.setVisibility(View.VISIBLE);
            return;
        }
        printLog(currentPost.toString());
        if (currentPost.getAuthor().getImageLink() != null) {
            Picasso.with(getActivity())
                    .load("http://" + getString(R.string.server) + "/image/" + currentPost.getAuthor().getImageLink())
                    .resize(400, 400)
                    .centerCrop()
                    .into(authorImage);
        } else {
            authorImage.setImageResource(R.drawable.no_photo);
        }
        Picasso.with(getActivity())
                .load("http://" + getString(R.string.server) + "/image/" + currentPost.getLeftCard().getImageLink())
                .resize(800, 800)
                .centerCrop()
                .placeholder(R.drawable.prbar)
                .error(R.drawable.err)
                .into(leftCardImage);
        Picasso.with(getActivity())
                .load("http://" + getString(R.string.server) + "/image/" + currentPost.getRightCard().getImageLink())
                .resize(800, 800)
                .centerCrop()
                .placeholder(R.drawable.prbar)
                .error(R.drawable.err)
                .into(rightCardImage);
        authorNickname.setText(currentPost.getAuthor().getNickname());
        leftCardDescription.setText(currentPost.getLeftCard().getDescription());
        rightCardDescription.setText(currentPost.getRightCard().getDescription());
        postDescription.setText(currentPost.getDescription());
    }

    @OnClick(R.id.try_again_btn)
    public void onTryAgainClick() {
        ((MainActivity) getActivity()).update();
    }

    @Override
    public void onResume() {
        super.onResume();
        getPresenter().onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        singleClicks.unsubscribe();
        doubleClicks.unsubscribe();
    }

    private class cardClickEvent {
        private final ImageView cardImage;
        private final ImageView likeImage;
        private final int numberOfClicks;
        private final boolean isLeft;

        public cardClickEvent(ImageView cardImage, ImageView likeImage, int numberOfClicks, boolean isLeft) {
            this.cardImage = cardImage;
            this.likeImage = likeImage;
            this.numberOfClicks = numberOfClicks;
            this.isLeft = isLeft;
        }
    }
}
