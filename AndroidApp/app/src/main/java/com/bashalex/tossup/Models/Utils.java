package com.bashalex.tossup.Models;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.CursorLoader;
import android.database.Cursor;
import android.graphics.Matrix;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Size;
import android.util.TypedValue;
import android.view.Surface;
import android.view.WindowManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Alex Bash on 02.03.16.
 */
public class Utils {

    private static Point cardSize;
    private static Size previewSize;

    public static Point computeSizeForCards(Context c) {
        if (cardSize != null) return cardSize;
        DisplayMetrics displayMetrics = c.getResources().getDisplayMetrics();
        int screenHeight = displayMetrics.heightPixels;
        int screenWidth = displayMetrics.widthPixels;
        int marginsWidth = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                16 * 3, c.getResources().getDisplayMetrics()));
        int cardWidth = Math.round((screenWidth - marginsWidth) / 2.f);
        int cardHeight = Math.round(cardWidth * 1.5f);
        if (cardHeight > screenHeight / 2) {
            cardHeight = screenHeight / 2;
            cardWidth = Math.round(cardHeight / 1.5f);
        }
        cardSize = new Point(cardWidth, cardHeight);
        return cardSize;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static Size chooseOptimalPreviewSize(Activity activity, Size[] choices, Size aspectRatio) {
        if (previewSize != null) return previewSize;
        Point displaySize = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(displaySize);

        int maxPreviewWidth = displaySize.x;
        int maxPreviewHeight = displaySize.y;

        if (maxPreviewWidth > 1080) {
            maxPreviewWidth = 1080;
        }

        if (maxPreviewHeight > 1920) {
            maxPreviewHeight = 1920;
        }

        // Collect the supported resolutions that are at least as big as the preview Surface
        List<Size> smallEnough = new ArrayList<>();
        int w = aspectRatio.getWidth();
        int h = aspectRatio.getHeight();
        for (Size option : choices) {
            if (option.getHeight() <= maxPreviewWidth &&
                    option.getWidth() <= maxPreviewHeight &&
                    option.getWidth() == option.getHeight() * w / h) {
                smallEnough.add(option);
            }
        }

        if (smallEnough.size() > 0) {
            previewSize = Collections.max(smallEnough, new CompareSizesByArea());
            return previewSize;
        } else {
            return choices[0];
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static class CompareSizesByArea implements Comparator<Size> {
        @Override
        public int compare(Size lhs, Size rhs) {
            return Long.signum((long) lhs.getWidth() * lhs.getHeight() -
                    (long) rhs.getWidth() * rhs.getHeight());
        }
    }

    public static int getRelativeImageOrientation(int displayRotation, int sensorOrientation,
                                                  boolean isFrontFacing, boolean compensateForMirroring) {
        int result;
        if (isFrontFacing) {
            result = (sensorOrientation + displayRotation) % 360;
            if (compensateForMirroring) {
                result = (360 - result) % 360;
            }
        } else {
            result = (sensorOrientation - displayRotation + 360) % 360;
        }
        return result;
    }

    public static int getDisplayRotation(Context context) {
        WindowManager windowManager = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        int rotation = windowManager.getDefaultDisplay().getRotation();
        switch (rotation) {
            case Surface.ROTATION_0:
                return 0;
            case Surface.ROTATION_90:
                return 90;
            case Surface.ROTATION_180:
                return 180;
            case Surface.ROTATION_270:
                return 270;
        }
        return 0;
    }

    public static String getPath(Context context, Uri uri) {
        ContentResolver resolver = context.getContentResolver();
        String path;
        String[] fields = {MediaStore.Images.Media.DATA};
        Cursor cursor = resolver.query(uri, fields, null, null, null);

        if (cursor == null) {
            return uri.getPath();
        }

        cursor.moveToFirst();
        path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();
        return path;
    }
}
