package com.bashalex.tossup.Interfaces;

/**
 * Created by Alex Bash on 19.06.16.
 */
public interface RegistrationRedirectListener {
    void onLikeWithoutRegistration();
}
