package com.bashalex.tossup.Custom;

import android.Manifest;
import android.app.AlertDialog;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;

import com.bashalex.tossup.Interfaces.ActivityRequestedListener;
import com.bashalex.tossup.Models.TossUpCodes;
import com.bashalex.tossup.R;

import nucleus.presenter.Presenter;

/**
 * Created by Alex Bash on 21.02.16.
 */
public class TossUpPresenter extends Presenter {

    protected TossUpActivity mActivity;
    private String TAG;

    public void init(TossUpActivity activity) {
        mActivity = activity;
        TAG = getClass().getSimpleName();
    }

    public void destroy() {
        super.destroy();
        mActivity = null;
    }

    public void printLog(String message) {
        if (message == null) {
            Log.e(TAG, "attempt to print null message");
        } else {
            Log.d(TAG, message);
        }
    }

    public boolean checkGalleryPermissions() {
        if (ContextCompat.checkSelfPermission(mActivity,
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
                mActivity.showToast(mActivity.getString(R.string.request_gallery_permissions));
            } else {
                printLog("request permissions for gallery");
                ActivityCompat.requestPermissions(mActivity,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        TossUpCodes.getCameraPermissions);
            }
        } else {
            return true;
        }
        return false;
    }

    public void showCameraDialog(View view, ActivityRequestedListener listener) {
        AlertDialog.Builder ad = new AlertDialog.Builder(mActivity, R.style.TossUpAlertDialogTheme);
        ad.setTitle(mActivity.getString(R.string.dialog_title));
        ad.setPositiveButton(mActivity.getString(R.string.dialog_upload),
                (dialog, arg1) -> {
                    switch (view.getId()) {
                        case R.id.left_card:
                            listener.onGalleryRequested(TossUpCodes.leftCardGallery);
                            break;
                        case R.id.right_card:
                            listener.onGalleryRequested(TossUpCodes.rightCardGallery);
                            break;
                        default:
                            listener.onGalleryRequested(TossUpCodes.avatarGallery);
                            break;
                    }});
        ad.setNegativeButton(mActivity.getString(R.string.dialog_make_now),
                (dialog, arg1) -> {
                    switch (view.getId()) {
                        case R.id.left_card:
                            listener.onCameraScreenRequested(TossUpCodes.leftCardCamera);
                            break;
                        case R.id.right_card:
                            listener.onCameraScreenRequested(TossUpCodes.rightCardCamera);
                            break;
                        default:
                            listener.onCameraScreenRequested(TossUpCodes.avatarCamera);
                            break;
                    }});
        ad.setCancelable(true);
        ad.show();
    }
}
