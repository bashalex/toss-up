package com.bashalex.tossup.Interfaces;

import android.view.View;

import com.bashalex.tossup.Models.Comment;

public interface CommentClickListener {
    void onCommentClick(Comment comment, View view);
}