package com.bashalex.tossup.Interfaces;

/**
 * Created by Alex Bash on 02.03.16.
 */
public interface QuestionUploadedListener {
    void onQuestionUploaded();
}
