package com.bashalex.tossup.Presenters;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bashalex.tossup.API.AccountResponse;
import com.bashalex.tossup.API.DefaultResponse;
import com.bashalex.tossup.API.PostsResponse;
import com.bashalex.tossup.Adapters.UserPostsAdapter;
import com.bashalex.tossup.Custom.TossUpActivity;
import com.bashalex.tossup.Custom.TossUpPresenter;
import com.bashalex.tossup.Fragments.FragmentProfile;
import com.bashalex.tossup.Interfaces.PhotoUpdateListener;
import com.bashalex.tossup.Interfaces.RecentPostsUpdateListener;
import com.bashalex.tossup.Interfaces.RegistrationStepChangeListener;
import com.bashalex.tossup.Models.Account;
import com.bashalex.tossup.Models.Authorisation;
import com.bashalex.tossup.Models.ServerRequests;

import io.realm.Realm;
import rx.Subscription;
import rx.observables.ConnectableObservable;

/**
 * Created by Alex Bash on 21.02.16.
 */
public class PresenterProfile extends TossUpPresenter {

    private Account account;
    private Realm realm;

    private Subscription subscr;
    private PhotoUpdateListener photoUpdateListener;
    private RecentPostsUpdateListener recentPostsUpdateListener;
    private UserPostsAdapter adapter;

    public void init(TossUpActivity activity) {
        super.init(activity);
        printLog("PRESENTER_PROFILE INIT");
        realm = Realm.getDefaultInstance();
        account = realm.where(Account.class).findFirst();
    }

    public Account getAccount() {
        return account;
    }

    public void destroy() {
        onDestroyView();
        super.destroy();
    }

    public void onDestroyView() {
        photoUpdateListener = null;
        recentPostsUpdateListener = null;
        adapter = null;
        realm.close();
    }

    public void setListeners(FragmentProfile listener) {
        photoUpdateListener =  listener;
        recentPostsUpdateListener = listener;
    }

    public void logout() {
        if (mActivity instanceof RegistrationStepChangeListener) {
            Authorisation.logout(mActivity, (RegistrationStepChangeListener) mActivity);
        } else {
            throw new RuntimeException(mActivity.toString()
                    + " must implement RegistrationStepChangeListener");
        }
    }

    public void updateRecentPosts(RecyclerView view) {
        if (null == adapter) {
            printLog("adapter is null");
            setAdapter(view);
        } else {
            ConnectableObservable<DefaultResponse<PostsResponse>> connection =
                    ServerRequests.getUserPosts(mActivity, Authorisation.getToken(mActivity));
            if (subscr != null && !subscr.isUnsubscribed()) subscr.unsubscribe();
            subscr = connection.filter(response -> response.getErrorCode() == 0)
                    .subscribe(response -> {
                        adapter.mPosts = response.getData().getPosts();
                        adapter.notifyDataSetChanged();
                        recentPostsUpdateListener.onPostsUpdated(adapter.mPosts.size());
                    });
            connection.connect();
        }
    }

    public void updatePhoto(Uri photo) {
        printLog("Uri: " + photo.toString());
        ConnectableObservable<DefaultResponse<AccountResponse>> connection =
                ServerRequests.updatePhoto(mActivity, photo, Authorisation.getToken(mActivity));
        if (subscr != null && !subscr.isUnsubscribed()) subscr.unsubscribe();
        subscr = connection.filter(response -> response.getErrorCode() == 0)
                .subscribe(response -> {
                    realm.beginTransaction();
                    printLog("response: " + response.getData().toString());
                    AccountResponse newAccount = response.getData();
                    account.setImageLink(newAccount.getImageLink());
                    account.setEmail(newAccount.getEmail());
                    account.setLast_visited_post(newAccount.getLast_visited_post());
                    account.setNickname(newAccount.getNickname());
                    account.setStatus(newAccount.getStatus());
                    realm.commitTransaction();
                    photoUpdateListener.onPhotoUpdated();
                });
        connection.connect();
    }

    private void setAdapter(View view) {
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);
            recyclerView.setLayoutManager(mLayoutManager);
            ConnectableObservable<DefaultResponse<PostsResponse>> connection =
                    ServerRequests.getUserPosts(mActivity, Authorisation.getToken(mActivity));
            if (subscr != null && !subscr.isUnsubscribed()) subscr.unsubscribe();
            subscr = connection.filter(response -> response.getErrorCode() == 0)
                    .subscribe(response -> {
                        printLog("onResponse()");
                        adapter = new UserPostsAdapter(context, response.getData().getPosts());
                        recyclerView.setAdapter(adapter);
                        printLog("adapter set");
                        recentPostsUpdateListener.onPostsUpdated(adapter.mPosts.size());
                    });
            connection.connect();
        }
    }

    public void changeStatus(String newStatus) {
        if (newStatus.equals(account.getStatus())) {
            return;
        }
        ConnectableObservable<DefaultResponse<Object>> connection =
        ServerRequests.updateStatus(mActivity, newStatus, Authorisation.getToken(mActivity));
        if (subscr != null && !subscr.isUnsubscribed()) subscr.unsubscribe();
        subscr = connection.filter(response -> response.getErrorCode() == 0)
                .subscribe(response -> {
                    mActivity.showToast("New status saved");
                });
        connection.connect();
    }
}
