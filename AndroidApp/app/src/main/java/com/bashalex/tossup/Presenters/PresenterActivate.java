package com.bashalex.tossup.Presenters;

import com.bashalex.tossup.Custom.TossUpPresenter;
import com.bashalex.tossup.Interfaces.RegistrationStepChangeListener;
import com.bashalex.tossup.Models.Authorisation;

/**
 * Created by bashalex on 22.02.16.
 */
public class PresenterActivate extends TossUpPresenter {

    public void activate(String code) {
        if (mActivity instanceof RegistrationStepChangeListener) {
            Authorisation.validateCode(mActivity, code,
                    (RegistrationStepChangeListener) mActivity);
        } else {
            throw new RuntimeException(mActivity.toString()
                    + " must implement RegistrationStepChangeListener");
        }
    }

    public void login() {
        if (mActivity instanceof RegistrationStepChangeListener) {
            Authorisation.login(mActivity, (RegistrationStepChangeListener) mActivity);
        } else {
            throw new RuntimeException(mActivity.toString()
                    + " must implement RegistrationStepChangeListener");
        }
    }

    public void back() {
        if (mActivity instanceof RegistrationStepChangeListener) {
            Authorisation.startRegistration(mActivity, (RegistrationStepChangeListener) mActivity);
        } else {
            throw new RuntimeException(mActivity.toString()
                    + " must implement RegistrationStepChangeListener");
        }
    }
}
