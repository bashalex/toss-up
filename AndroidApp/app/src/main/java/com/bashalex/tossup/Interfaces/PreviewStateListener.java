package com.bashalex.tossup.Interfaces;

import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.SurfaceTexture;
import android.net.Uri;

/**
 * Created by Alex Bash on 02.03.16.
 */
public interface PreviewStateListener {
    void onOrientationChanged(Matrix matrix);
    void onRatioChanged(Point previewSize);
    SurfaceTexture onStartPreview();
    void onImageSaved(Uri imageUri);
}
