package com.bashalex.tossup.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bashalex.tossup.Custom.TossUpFragment;
import com.bashalex.tossup.Interfaces.PhotoUpdateListener;
import com.bashalex.tossup.Interfaces.RecentPostsUpdateListener;
import com.bashalex.tossup.Models.Account;
import com.bashalex.tossup.Models.TossUpCodes;
import com.bashalex.tossup.Presenters.PresenterProfile;
import com.bashalex.tossup.R;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import nucleus.factory.RequiresPresenter;

@RequiresPresenter(PresenterProfile.class)
public class FragmentProfile extends TossUpFragment<PresenterProfile>
                             implements PhotoUpdateListener, RecentPostsUpdateListener {

    @Bind(R.id.profile_photo)
    CircleImageView profilePhoto;
    @Bind(R.id.profile_nickname)
    TextView nicknameField;
    @Bind(R.id.profile_about)
    EditText statusField;
    @Bind(R.id.posts_list)
    RecyclerView postsView;
    @Bind(R.id.no_question_placeholder)
    RelativeLayout placeholder;

    public FragmentProfile() { }

    public static FragmentProfile newInstance() {
        return new FragmentProfile();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        printLog("FRAGMENT_PROFILE ON_CREATE_VIEW");
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);
        setStatusOnEditListener();
        return view;
    }

    public void onStart() {
        printLog("FRAGMENT_PROFILE ON_START");
        super.onStart();
        initViews();
    }

    private void initViews() {
        updateFields();
        getPresenter().updateRecentPosts(postsView);
    }

    private void updateFields() {
        Account account = getPresenter().getAccount();
        getPresenter().setListeners(this);
        if (account == null) {
            showToast("Something went wrong");
            getPresenter().logout();
        }
        if (account != null && account.getImageLink() != null) {
            printLog("load image...");
            Picasso.with(getActivity())
                    .load("http://" + getString(R.string.server) + "/image/" + account.getImageLink())
                    .resize(600, 600)
                    .centerCrop()
                    .into(profilePhoto);
        }
        nicknameField.setText(account != null ? account.getNickname() : null);
        statusField.setText(account != null ? account.getStatus() : null);
    }

    private void setStatusOnEditListener() {
        statusField.setOnEditorActionListener((v, actionId, event) -> {
            boolean handled = false;
            if (actionId == EditorInfo.IME_ACTION_SEND) {
                getPresenter().changeStatus(statusField.getText().toString());
                handled = true;
            }
            InputMethodManager inputManager =
                    (InputMethodManager) getActivity().
                            getSystemService(Context.INPUT_METHOD_SERVICE);
            View focus = getActivity().getCurrentFocus();
            if (focus != null) {
                inputManager.hideSoftInputFromWindow(
                        focus.getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
            }
            statusField.clearFocus();
            return handled;
        });
    }

    @OnClick(R.id.logout_button)
    public void logout() {
        getPresenter().logout();
    }

    @OnClick(R.id.profile_photo)
    public void onProfilePhotoClick(View view) {
        showCameraDialog(view);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == TossUpCodes.avatarCamera ||
                    requestCode == TossUpCodes.avatarGallery) {
                Uri avatar = data.getData();
                getPresenter().updatePhoto(avatar);
            }
        } else {
            showToast(getString(R.string.choose_image));
        }
    }

    @Override
    public void onPhotoUpdated() {
        updateFields();
        showToast(getString(R.string.photo_updated));
    }

    @Override
    public void onPostsUpdated(int numOfPosts) {
        printLog(String.valueOf(numOfPosts));
        if (postsView != null) {
            postsView.setVisibility(numOfPosts == 0 ? View.GONE : View.VISIBLE);
        }
        if (placeholder != null) {
            placeholder.setVisibility(numOfPosts == 0 ? View.VISIBLE : View.GONE);
        }
    }

    public void onDestroyView() {
        printLog("FRAGMENT_PROFILE ON_CREATE_VIEW");
        getPresenter().onDestroyView();
        super.onDestroyView();
    }
}
