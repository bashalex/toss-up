package com.bashalex.tossup.Presenters;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.TextureView;

import com.bashalex.tossup.Custom.TossUpActivity;
import com.bashalex.tossup.Custom.TossUpPresenter;
import com.bashalex.tossup.Interfaces.CameraInterface;
import com.bashalex.tossup.Models.CameraModel;
import com.bashalex.tossup.Models.TossUpCodes;
import com.bashalex.tossup.R;

/**
 * Created by Alex Bash on 01.03.16.
 */
public class PresenterCamera extends TossUpPresenter {

    private CameraInterface tossUpCamera;

    public void init(TossUpActivity activity) {
        super.init(activity);
        printLog("init");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tossUpCamera = new PresenterCameraNew(mActivity);
        } else {
            tossUpCamera = new PresenterCameraOld(mActivity);
        }
    }

    public void checkPermissions() {
        if (ContextCompat.checkSelfPermission(mActivity,
                Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity,
                    Manifest.permission.CAMERA)) {
                mActivity.showToast(mActivity.getString(R.string.request_camera_permissions));
            } else {
                printLog("request permissions for camera");
                ActivityCompat.requestPermissions(mActivity,
                        new String[]{Manifest.permission.CAMERA},
                        TossUpCodes.getCameraPermissions);
            }
        }
    }

    public void openCamera(TextureView previewView) {
        if (tossUpCamera != null) {
            printLog("openCamera");
            tossUpCamera.openCamera(previewView.getWidth(), previewView.getHeight(), CameraModel.CAMERA_FRONT);
        }
    }

    public void switchCamera() {
        if (tossUpCamera != null) {
            printLog("switchCamera");
            tossUpCamera.switchCamera();
        }
    }

    public void configureTransform(int width, int height) {
        if (tossUpCamera != null) {
            printLog("configureTransform");
            tossUpCamera.configureTransform(width, height);
        }
    }

    public void closeCamera() {
        if (tossUpCamera != null) {
            printLog("closeCamera");
            tossUpCamera.closeCamera();
        }
    }


    public void onDestroy() {
        printLog("onDestroy");
        if (tossUpCamera != null) {
            super.onDestroy();
            tossUpCamera.onDestroy();
        }
    }

    public void takePicture() {
        if (tossUpCamera != null) {
            printLog("takePicture");
            tossUpCamera.takePicture();
        }
    }
}
