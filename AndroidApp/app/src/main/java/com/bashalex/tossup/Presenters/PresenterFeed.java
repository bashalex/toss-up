package com.bashalex.tossup.Presenters;

import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;

import com.bashalex.tossup.Activities.FullscreenCardActivity;
import com.bashalex.tossup.Custom.TossUpPresenter;
import com.bashalex.tossup.Interfaces.PostChangeListener;
import com.bashalex.tossup.Models.Post;
import com.bashalex.tossup.R;

public class PresenterFeed extends TossUpPresenter {
    private boolean UIblocked = true;

    public void openFullscreenCard(View cardImage, Post post, boolean left) {
        if (!UIblocked) {
            UIblocked = true;
            Pair<View, String> holderPair = Pair.create(cardImage, "tHolder");
            @SuppressWarnings("unchecked")
            ActivityOptionsCompat activityOptions = ActivityOptionsCompat
                    .makeSceneTransitionAnimation(mActivity, holderPair);
            Intent intent = new Intent(mActivity, FullscreenCardActivity.class);
            intent.putExtra(mActivity.getString(R.string.intentArg1), post);
            intent.putExtra(mActivity.getString(R.string.intentArg2), left);
            mActivity.startActivity(intent, activityOptions.toBundle());
        }
    }

    public void onResume() {
        UIblocked = false;
    }

    public void likePost(View view, boolean isLeft) {
        if (!UIblocked) {
            UIblocked = true;
            AlphaAnimation animation = new AlphaAnimation(0, 1);
            animation.setDuration(500);

            animation.setAnimationListener(new AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    view.setVisibility(View.GONE);
                    likeAndOpenNextPost(isLeft);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });

            view.setVisibility(View.VISIBLE);
            view.setAnimation(animation);
        }
    }

    private void likeAndOpenNextPost(boolean isLeft) {
        if (mActivity instanceof PostChangeListener) {
            ((PostChangeListener) mActivity).onLikePost(isLeft);
        } else {
            throw new RuntimeException(mActivity.toString()
                    + " must implement PostChangeListener");
        }
    }
}
