package com.bashalex.tossup.API;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alex Bash on 13.06.16.
 */
public class AccountResponse {

    private String nickname;
    @SerializedName("image_link")
    private String imageLink;
    private String status;
    private String email;
    private int last_visited_post;

    public AccountResponse(String imageLink, String nickname, String status, String email, int last_visited_post) {
        this.imageLink = imageLink;
        this.nickname = nickname;
        this.email = email;
        this.status = status;
        this.last_visited_post = last_visited_post;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public int getLast_visited_post() {
        return last_visited_post;
    }

    public void setLast_visited_post(int last_visited_post) {
        this.last_visited_post = last_visited_post;
    }

    @Override
    public String toString() {
        return "AccountResponse{" +
                "nickname='" + nickname + '\'' +
                ", imageLink='" + imageLink + '\'' +
                ", status='" + status + '\'' +
                ", email='" + email + '\'' +
                ", last_visited_post=" + last_visited_post +
                '}';
    }
}
