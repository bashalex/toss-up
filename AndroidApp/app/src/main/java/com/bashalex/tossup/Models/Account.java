package com.bashalex.tossup.Models;

import com.bashalex.tossup.API.AccountResponse;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Alex Bash on 21.02.16.
 */
public class Account extends RealmObject {

    @PrimaryKey
    private String nickname;
    private String imageLink;
    private String status;
    private String email;
    private int last_visited_post;

    public Account(AccountResponse acc) {
        this.imageLink = acc.getImageLink();
        this.nickname = acc.getNickname();
        this.email = acc.getEmail();
        this.status = acc.getStatus();
        this.last_visited_post = acc.getLast_visited_post();
    }

    public Account() {}

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public int getLast_visited_post() {
        return last_visited_post;
    }

    public void setLast_visited_post(int last_visited_post) {
        this.last_visited_post = last_visited_post;
    }
}
