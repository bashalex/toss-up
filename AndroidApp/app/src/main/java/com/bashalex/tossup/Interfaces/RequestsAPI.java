package com.bashalex.tossup.Interfaces;

import com.bashalex.tossup.API.AccountResponse;
import com.bashalex.tossup.API.DefaultResponse;
import com.bashalex.tossup.API.PostsResponse;
import com.bashalex.tossup.API.TokenResponse;
import com.bashalex.tossup.Models.Post;

import okhttp3.RequestBody;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Alex Bash on 26.04.16.
 */
public interface RequestsAPI {
    @Multipart
    @POST("registration")
    Observable<DefaultResponse<TokenResponse>> registerNewAccount(@Part("email") String email,
                                                 @Part("nickname") String nickname,
                                                 @Part("password") String password,
                                                 @Part("photo\"; filename=\"image.png\" ") RequestBody image);

    @GET("registration/validate")
    Observable<DefaultResponse<Object>> validateEmail(@Header("token") String token,
                                             @Query("code") String code);

    @GET("authorisation/get_token")
    Observable<DefaultResponse<TokenResponse>> getToken(@Header("nickname") String nickname,
                                                        @Header("password") String password);

    @GET("authorisation")
    Observable<DefaultResponse<AccountResponse>> login(@Header("token") String token);

    @Multipart
    @POST("feed/new")
    Observable<DefaultResponse<Object>> createPost(@Part("left_description") String left_description,
                                          @Part("left_image\"; filename=\"left_image.png\" ") RequestBody left_image,
                                          @Part("right_description") String right_description,
                                          @Part("right_image\"; filename=\"right_image.png\" ") RequestBody right_image,
                                          @Part("description") String description,
                                          @Header("token") String token);


    @GET("feed/next_post")
    Observable<DefaultResponse<Post>> getNextPost(@Header("token") String token);

    @GET("feed/like_and_next_post")
    Observable<DefaultResponse<Post>> likeAndGetNextPost(@Header("token") String token,
                                                         @Query("post_id") String postId,
                                                         @Query("is_left") Boolean isLeft);

    @GET("profile/posts")
    Observable<DefaultResponse<PostsResponse>> getUserPosts(@Header("token") String token);

    @Multipart
    @POST("profile/update_status")
    Observable<DefaultResponse<Object>> updateStatus(@Header("token") String token,
                                                     @Part("status") String status);

    @Multipart
    @POST("profile/update_photo")
    Observable<DefaultResponse<AccountResponse>> updatePhoto(@Header("token") String token,
                                                    @Part("photo\"; filename=\"image.jpg\" ") RequestBody image);

}