package com.bashalex.tossup.Interfaces;

/**
 * Created by Alex Bash on 01.03.16.
 */
public interface ActivityRequestedListener {
    void onGalleryRequested(int code);
    void onCameraScreenRequested(int code);
}
