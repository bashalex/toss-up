package com.bashalex.tossup.Interfaces;

/**
 * Created by Alex Bash on 21.02.16.
 */
public interface RegistrationStepChangeListener {
    void onNextStep();
}
