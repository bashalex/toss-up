package com.bashalex.tossup.Interfaces;

/**
 * Created by Alex Bash on 20.06.16.
 */
public interface RecentPostsUpdateListener {
    void onPostsUpdated(int numOfPosts);
}
