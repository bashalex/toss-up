package com.bashalex.tossup.Custom;

import android.app.Application;
import android.util.Log;

import com.bashalex.tossup.R;
import com.squareup.leakcanary.LeakCanary;

import java.io.File;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Alex Bash on 21.02.16.
 */
public class TossUpApplication extends Application {

    public void onCreate() {
        super.onCreate();
        LeakCanary.install(this);
        RealmConfiguration config = new RealmConfiguration.Builder(this)
                .name("tossup.realm")
                .schemaVersion(1)
                .build();
        Realm.setDefaultConfiguration(config);
        clearApplicationData();
    }

    public void clearApplicationData() {
        /**
         * Delete temporary files which have been used for sending
         * images between activities
         */
        File cache = getCacheDir();
        if (cache.exists()) {
            for (File children : cache.listFiles()) {
                if (children.getName().contains(getString(R.string.temp_image))) {
                    children.delete();
                    Log.d("Application", children.toString() + " deleted");
                }
            }
        }
    }
}
