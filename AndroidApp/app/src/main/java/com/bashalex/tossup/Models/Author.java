package com.bashalex.tossup.Models;

import android.os.Parcel;
import android.os.Parcelable;

public class Author implements Parcelable {
    public static final Parcelable.Creator CREATOR
            = new Parcelable.Creator() {
        public Author createFromParcel(Parcel in) {
            return new Author(in);
        }

        public Author[] newArray(int size) {
            return new Author[size];
        }
    };
    private String nickname;
    private String image_link;

    public Author(String nickname, String image_link) {
        this.nickname = nickname;
        this.image_link = image_link;
    }

    private Author(Parcel in) {
        nickname = in.readString();
        image_link = in.readString();
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getImageLink() {
        return image_link;
    }

    public void setImageLink(String image_link) {
        this.image_link = image_link;
    }

    @Override
    public String toString() {
        return "Author{" +
                "nickname='" + nickname + '\'' +
                ", image_link='" + image_link + '\'' +
                '}';
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(nickname);
        out.writeString(image_link);
    }
}
