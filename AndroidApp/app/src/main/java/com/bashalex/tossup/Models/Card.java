package com.bashalex.tossup.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Alex Bash on 18.02.16.
 */
public class Card implements Parcelable {
    public static final Parcelable.Creator CREATOR
            = new Parcelable.Creator() {
        public Card createFromParcel(Parcel in) {
            return new Card(in);
        }

        public Card[] newArray(int size) {
            return new Card[size];
        }
    };
    private String image_link;
    private String description;
    private int number_of_likes;

    public Card(String imageLink, String description, int number_of_likes) {
        this.image_link = imageLink;
        this.description = description;
        this.number_of_likes = number_of_likes;
    }

    private Card(Parcel in) {
        description = in.readString();
        image_link = in.readString();
        number_of_likes = in.readInt();
    }

    public String getImageLink() {
        return image_link;
    }

    public void setImageLink(String imageLink) {
        this.image_link = imageLink;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNumberOfLikes() {
        return number_of_likes;
    }

    public void setNumberOfLikes(int number_of_likes) {
        this.number_of_likes = number_of_likes;
    }

    @Override
    public String toString() {
        return "Card{" +
                "image_link='" + image_link + '\'' +
                ", description='" + description + '\'' +
                ", number_of_likes=" + number_of_likes +
                '}';
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(description);
        out.writeString(image_link);
        out.writeInt(number_of_likes);
    }
}
