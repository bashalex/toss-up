package com.bashalex.tossup.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bashalex.tossup.Interfaces.CommentClickListener;
import com.bashalex.tossup.Models.Comment;
import com.bashalex.tossup.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.ViewHolder> {

    private final CommentClickListener mListener;
    public List<Comment> mComments;
    private Context mContext;

    public CommentsAdapter(Context context, CommentClickListener listener,
                           List<Comment> comments) {
        mListener = listener;
        mContext = context;
        mComments = comments;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_comment, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Comment comment = mComments.get(position);
        holder.comment = comment;
        holder.authorName.setText(comment.getAuthor().getNickname());
        holder.message.setText(comment.getText());
        Picasso.with(mContext)
                .load(comment.getAuthor().getImageLink())
                .into(holder.authorImage);
        holder.view.setOnClickListener(v -> {
            if (null != mListener) {
                mListener.onCommentClick(holder.comment, holder.view);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mComments.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View view;
        public Comment comment;
        @Bind(R.id.comment_author_name)
        TextView authorName;
        @Bind(R.id.comment_message)
        TextView message;
        @Bind(R.id.comment_author_image)
        ImageView authorImage;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            this.view = view;
        }
    }
}