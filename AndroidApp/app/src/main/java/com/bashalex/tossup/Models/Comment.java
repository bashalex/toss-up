package com.bashalex.tossup.Models;

/**
 * Created by Alex Bash on 18.02.16.
 */
public class Comment {
    private String text;
    private Author author;
    private int id;

    public Comment(String text, Author author, int id) {
        this.text = text;
        this.author = author;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "text='" + text + '\'' +
                ", author=" + author +
                '}';
    }

}
