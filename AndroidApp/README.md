### Requirements:
 - Android studio 2.0.+
 

Before building application it's necessary to set correct server address in file ./app/src/main/res/values/strings.xml
If you ran server with nginx it works on the standard 80/443 ports,
but in case of running only with flask, port 5000 should be specified.
 